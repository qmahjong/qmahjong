TEMPLATE = subdirs
include(qmahjong.subdirs.pri)
CONFIG += ordered
SUBDIRS = \
    mahjong \
    rules \
    Model \
    Gui \
    main

CONFIG(debug, debug|release){
    SUBDIRS += tests
}

OTHER_FILES += qmahjong.pri \
    qmahjong.subdirs.pri
