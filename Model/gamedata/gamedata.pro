# -------------------------------------------------
# Project created by QtCreator 2009-11-28T15:19:45
# -------------------------------------------------
QT -= gui
TARGET = gamedata
TEMPLATE = lib
CONFIG += staticlib
include(../../qmahjong.pri)

SOURCES += \
    globalsettings.cpp
HEADERS += \
    globalsettings.h
HEADERS += \
    ../../include/Model/GameData
