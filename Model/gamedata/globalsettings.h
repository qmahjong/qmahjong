#ifndef GLOBALSETTINGS_H
#define GLOBALSETTINGS_H

#include <QSettings>

/*!
  \brief Global game settings.
*/
class GlobalSettings: private QSettings
{
    Q_OBJECT

public:
    GlobalSettings();
    ~GlobalSettings();

    bool getDebugMenu() const;
    void setDebugMenu(bool state);
    QString getPlayerName() const;
    void setPlayerName(QString name);
};

#endif // GLOBALSETTINGS_H
