#include "globalsettings.h"

GlobalSettings::GlobalSettings() :
        QSettings()
{
}

GlobalSettings::~GlobalSettings()
{
}

bool GlobalSettings::getDebugMenu() const
{
    return value("debug_menu", false).toBool();
}

void GlobalSettings::setDebugMenu(bool state)
{
    setValue("debug_menu", state);
}

QString GlobalSettings::getPlayerName() const
{
    return value("Player/name", tr("New Player")).toString();
}

void  GlobalSettings::setPlayerName(QString name)
{
    setValue("Player/name", name);
}
