#include "gameflow.h"
#include <QtTest/QtTest>

#include "Mahjong"

GameFlow::GameFlow(QObject *parent) :
    QObject(parent)
{
}

void GameFlow::rulesPlugin_data()
{
    QTest::addColumn<QString>("shortName");

    QTest::newRow("Riichi Rules") << "Riichi";
}

void GameFlow::rulesPlugin()
{
    QFETCH(QString, shortName);

    MahjongRulesInterface *rule = 0;

    foreach(MahjongRulesInterface* plugin, gMahjong->getRules())
    {
        if(plugin->rulesShortName() == shortName)
            rule = plugin;
    }

    QVERIFY2(rule, "Plugin not found");
    QVERIFY(!rule->rulesName().isEmpty());
    QVERIFY(!rule->rulesDesc().isEmpty());
}

void GameFlow::simpleGameFlow()
{
    /*
    PlayingMat *mat = new PlayingMat();
    Player *east = new Player("East", Player::EAST, mat);
    Player *north = new Player("North", Player::NORTH, mat);
    Player *west = new Player("West", Player::WEST, mat);
    Player *south = new Player("South", Player::SOUTH, mat);
    */
    QVERIFY(true);
}

QTEST_MAIN(GameFlow)
