# -------------------------------------------------
# Project created by QtCreator 2009-12-14T10:12:39
# -------------------------------------------------
TARGET = gameflow
DEPLIBS = mahjong
QT -= gui
include(../tests.pri)
include(../../qmahjong.pri)

SOURCES += gameflow.cpp
HEADERS += gameflow.h
