#ifndef GAMEFLOW_H
#define GAMEFLOW_H

#include <QObject>

class MahjongRulesInterface;

class GameFlow : public QObject
{
    Q_OBJECT

public:
    explicit GameFlow(QObject *parent = 0);

private slots:
    void rulesPlugin_data();
    void rulesPlugin();
    void simpleGameFlow();
};

#endif // GAMEFLOW_H
