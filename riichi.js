function rollDie()
{
    return Math.floor(Math.random()*6) + 1;
}

function setUpGameWidget()
{
    var widget = gameWidget.loadWidget('riichi.ui');

    for (var i = 0; i < 4; ++i)
    {
        widget.findChild('playerBox' + i).title = 'Player ' + (i + 1);
        game.player(i).nameChanged.connect(widget.findChild('nameLabel' + i).setText);
    }

    widget.findChild('seat0').text = 'East';
    game.player(0).seat = Player.EAST;
    game.player(0).name = "Maxime";

    widget.findChild('seat1').text = 'North';
    game.player(1).seat = Player.NORTH;
    game.player(1).name = "Computer 1";

    widget.findChild('seat2').text = 'West';
    game.player(2).seat = Player.WEST;
    game.player(2).name = "Computer 2";

    widget.findChild('seat3').text = 'South';
    game.player(3).seat = Player.SOUTH;
    game.player(3).name = "Computer 3";
}

function setUpGameTiles()
{
    var mat = game.mat;
    var tiles = mat.createSuitOf(Tile.CIRCLE);
    tiles = tiles.concat(mat.createSuitOf(Tile.BAMBOO));
    tiles = tiles.concat(mat.createSuitOf(Tile.CHARACTER));
    tiles = tiles.concat(mat.createSuitOf(Tile.WIND));
    tiles = tiles.concat(mat.createSuitOf(Tile.DRAGON));
    if (0)
    {
        tiles = tiles.concat(mat.createSuitOf(Tile.SEASON));
        tiles = tiles.concat(mat.createSuitOf(Tile.FLOWER));
    }

    mat.wall.tiles = tiles;
    mat.wall.shuffle();
}

function breakTheWall()
{
    var die = rollDie() + rollDie() - 1;
    var wallLength = game.mat.wall.tiles.length;
    var startPoint = wallLength/4 * ((4 * die - die) % 4);
    var breakPoint = startPoint + die * 2;

    game.mat.wall.breakWall(breakPoint);
}

function distributeTiles()
{
    var time = 1000;
    for (var a = 0; a < 4; ++a)
    {
        for (var i = 0; i < 4; ++i)
            game.player(0).doDraw();

        for (var i = 0; i < 4; ++i)
            game.player(3).doDraw();

        for (var i = 0; i < 4; ++i)
            game.player(2).doDraw();

        for (var i = 0; i < 4; ++i)
            game.player(1).doDraw();
    }

    game.player(0).doDraw();
    game.player(3).doDraw();
    game.player(2).doDraw();
    game.player(1).doDraw();
    game.player(0).doDraw();
}

setUpGameWidget();
setUpGameTiles();

game.mat.wall.breakWallRequested.connect(breakTheWall);
game.mat.prepared.connect(distributeTiles);
