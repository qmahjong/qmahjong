#ifndef PLAYERSETTINGSPAGE_H
#define PLAYERSETTINGSPAGE_H

#include <QWidget>
#include <QScopedPointer>

namespace Ui
{
    class PlayerSettingsPage;
}

/*!
  \brief Player's settings.

  Configuration page for the player's settings.
*/
class PlayerSettingsPage : public QWidget
{
    Q_OBJECT

public:
    explicit PlayerSettingsPage(QWidget *parent = 0);
    ~PlayerSettingsPage();

public slots:
    void loadSettings();
    void saveSettings();

protected:
    void changeEvent(QEvent *e);

private:
    QScopedPointer<Ui::PlayerSettingsPage> ui;
};

#endif // PLAYERSETTINGSPAGE_H
