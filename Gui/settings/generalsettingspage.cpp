#include "generalsettingspage.h"
#include "ui_generalsettingspage.h"

#include "Model/GameData"

GeneralSettingsPage::GeneralSettingsPage(QWidget * parent) :
        QWidget(parent),
        ui(new Ui::GeneralSettingsPage)
{
    ui->setupUi(this);
    loadSettings();
}

GeneralSettingsPage::~GeneralSettingsPage()
{
}

void GeneralSettingsPage::loadSettings()
{
    GlobalSettings settings;
    ui->debugCheckBox->setChecked(settings.getDebugMenu());
}

void GeneralSettingsPage::saveSettings()
{
    GlobalSettings settings;
    settings.setDebugMenu(ui->debugCheckBox->checkState());
}

void GeneralSettingsPage::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
