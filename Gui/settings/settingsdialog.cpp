#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include "Mahjong"

#include <QPushButton>
#include <QDialogButtonBox>
#include <QRegExp>

SettingsDialog::SettingsDialog(QWidget *parent) :
        QDialog(parent),
        ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    // load additional settings page from the Mahjong instance
    foreach (MahjongRulesInterface *rules, gMahjong->getRules())
    {
        QWidget *rulePage = rules->getWidget(Mahjong::RW_SettingsPage);
        if (rulePage)
        {
            ui->listWidget->addItem(rules->rulesShortName());
            ui->stackedWidget->addWidget(rulePage);
        }
    }

    connect(ui->buttonBox->button(QDialogButtonBox::Apply), SIGNAL(clicked()), this, SIGNAL(accepted()));
    connect(ui->buttonBox->button(QDialogButtonBox::Discard), SIGNAL(clicked()), this, SIGNAL(rejected()));

    foreach (QWidget *page, ui->stackedWidget->findChildren<QWidget*>(QRegExp("SettingsPage$")))
    {
        connect(this, SIGNAL(accepted()), page, SLOT(saveSettings()));
        connect(this, SIGNAL(rejected()), page, SLOT(loadSettings()));
    }
}

SettingsDialog::~SettingsDialog()
{
}

void SettingsDialog::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
