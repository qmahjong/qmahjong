#include "playersettingspage.h"
#include "ui_playersettingspage.h"

#include "Model/GameData"

PlayerSettingsPage::PlayerSettingsPage(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::PlayerSettingsPage)
{
    ui->setupUi(this);
    loadSettings();
}

PlayerSettingsPage::~PlayerSettingsPage()
{
}

void PlayerSettingsPage::loadSettings()
{
    GlobalSettings settings;
    ui->nameEdit->setText(settings.getPlayerName());
}

void PlayerSettingsPage::saveSettings()
{
    GlobalSettings settings;
    settings.setPlayerName(ui->nameEdit->text());
}

void PlayerSettingsPage::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
