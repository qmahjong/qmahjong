#ifndef GENERALSETTINGSPAGE_H
#define GENERALSETTINGSPAGE_H

#include <QWidget>
#include <QScopedPointer>

namespace Ui
{
    class GeneralSettingsPage;
};

/*!
  \brief General settings.

  Configuration page for the general settings.
*/
class GeneralSettingsPage : public QWidget
{
    Q_OBJECT

public:
    explicit GeneralSettingsPage(QWidget *parent = 0);
    ~GeneralSettingsPage();

public slots:
    void loadSettings();
    void saveSettings();

protected:
    void changeEvent(QEvent *e);

private:
    QScopedPointer<Ui::GeneralSettingsPage> ui;
};

#endif // GENERALSETTINGSPAGE_H
