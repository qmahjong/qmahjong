# -------------------------------------------------
# Project created by QtCreator 2009-11-28T15:47:52
# -------------------------------------------------
TARGET = settings
TEMPLATE = lib
CONFIG += staticlib
include(../../qmahjong.pri)
SOURCES += settingsdialog.cpp \
    generalsettingspage.cpp \
    playersettingspage.cpp
HEADERS += settingsdialog.h \
    generalsettingspage.h \
    playersettingspage.h
FORMS += settingsdialog.ui \
    generalsettingspage.ui \
    playersettingspage.ui
HEADERS += ../../include/Gui/Settings
