# -------------------------------------------------
# Project created by QtCreator 2010-02-22T13:28:29
# -------------------------------------------------
TARGET = gamegraphics
TEMPLATE = lib
CONFIG += staticlib
include(../../qmahjong.pri)
SOURCES += gamegraphics.cpp \
    graphicstile.cpp \
    graphicswall.cpp \
    graphicsplayer.cpp \
    graphicshand.cpp
HEADERS += gamegraphics.h \
    graphicstile.h \
    graphicswall.h \
    graphicsplayer.h \
    graphicshand.h
HEADERS += ../../include/Gui/GameGraphics
