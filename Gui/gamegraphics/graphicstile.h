#ifndef GRAPHICSTILE_H
#define GRAPHICSTILE_H

#include "tile.h"

#include <QGraphicsObject>
#include <QList>

class GraphicsTile : public QGraphicsObject
{
    Q_OBJECT
    Q_PROPERTY(bool faceUp READ isFaceUp)
    Q_PROPERTY(int flipAngle READ flipAngle WRITE setFlipAngle)

public:
    explicit GraphicsTile(QGraphicsItem *parent = 0);
    explicit GraphicsTile(Tile *tile, QGraphicsItem *parent = 0);
    virtual ~GraphicsTile();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter,
                       const QStyleOptionGraphicsItem *option,
                       QWidget *widget);

    bool isFaceUp() const;
    int flipAngle() const; /*!< \sa flipAngle */

    static QSize staticSize();

    static GraphicsTile* fromTile(Tile *tile, QGraphicsItem *parent = 0);
    static QList<GraphicsTile*> fromTileList(const TileList &tiles,
                                             QGraphicsItem *parent = 0);

public slots:
    void setFlipAngle(int angle);
    void flip();

signals:
    void selected(Tile *tile);

protected:
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    void setText(const QString &text);

private:
    QGraphicsTextItem *m_text;
    int m_flipAngle;
    Tile *m_tile;

private slots:
    void on_tile_flipped(Tile::Facing faceUp);
};

#endif // GRAPHICSTILE_H
