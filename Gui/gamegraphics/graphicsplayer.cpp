#include "graphicsplayer.h"
#include "graphicstile.h"

#include "Mahjong"

#include <QGraphicsTextItem>
#include <QPainter>
#include <QMessageBox>

/*!
  \class GraphicsPlayer
  \ingroup GameGraphics

  \brief The GraphicsPlayer class is a graphical representation of a
  \link Player %Mahjong player\endlink.

  \sa GameGraphics
*/

/*!
  \property GraphicsPlayer::playerName

  This property holds the player's display name.
*/

/*!
  Construct a GraphicsPlayer which does not represent a player.
  This is useful for implementing \b NULL objects.
*/
GraphicsPlayer::GraphicsPlayer(QGraphicsItem *parent) :
        QGraphicsObject(parent),
        m_name(new QGraphicsTextItem(this)),
        m_player(0)
{
    m_name->setFont(QFont("Sans Serif", 12));
}

/*!
  Construct a GraphicsPlayer which represents the player \a player.
*/
GraphicsPlayer::GraphicsPlayer(Player *player, QGraphicsItem *parent) :
        QGraphicsObject(parent),
        m_name(new QGraphicsTextItem(this)),
        m_player(player)
{
    m_name->setFont(QFont("Sans Serif", 12));
    setPlayerName(player->name());
    connect(player, SIGNAL(nameChanged(QString)), SLOT(setPlayerName(QString)));

    if (player->isLocalHuman())
    {
        connect(player, SIGNAL(drawEntered()), SLOT(on_player_drawEntered()));
        connect(player, SIGNAL(discardEntered()), SLOT(on_player_discardEntered()));
        connect(player, SIGNAL(discardExited()), SLOT(on_player_discardExited()));
    }
}

/*!
  Destroy the GraphicsPlayer.
*/
GraphicsPlayer::~GraphicsPlayer()
{
}

QRectF GraphicsPlayer::boundingRect() const
{
    return m_name->mapRectToParent(m_name->boundingRect())
                   .adjusted(-6, -6, 6, 6);
}

void GraphicsPlayer::paint(QPainter *painter,
                           const QStyleOptionGraphicsItem * /*option*/,
                           QWidget * /*widget*/)
{
    painter->save();

    painter->setBrush(QBrush(QColor::fromRgbF(0.8, 0.8, 0.8, 0.5)));
    painter->drawRoundedRect(m_name->mapRectToParent(m_name->boundingRect())
                             .adjusted(-5, -5, 5, 5), 5, 5);

    painter->restore();
}

/*!
  \return the player's name.

  \sa setPlayerName
*/
QString GraphicsPlayer::playerName() const
{
    return m_name->toPlainText();
}

/*!
  Sets the player's display name to \a name.

  \sa playerName
*/
void GraphicsPlayer::setPlayerName(const QString &name)
{
    m_name->setHtml(name);
    m_name->setPos(-m_name->boundingRect().center());
    m_name->setTransformOriginPoint(m_name->boundingRect().center());
}

void GraphicsPlayer::on_player_drawEntered()
{
    QMessageBox::information(0,
        tr("Draw"),
        tr("Draw a tile."));

    m_player->draw();
}

void GraphicsPlayer::on_player_discardEntered()
{
    QMessageBox::information(0,
        tr("Discard"),
        tr("Select a tile to discard."));

    QList<GraphicsTile*> tiles =
            GraphicsTile::fromTileList(m_player->hand()->closedTiles());

    foreach (GraphicsTile *tile, tiles)
    {
        tile->setAcceptHoverEvents(true);
        connect(tile, SIGNAL(selected(Tile*)),
                SLOT(on_tile_selected(Tile*)));
    }
}

void GraphicsPlayer::on_player_discardExited()
{
    QList<GraphicsTile*> tiles =
            GraphicsTile::fromTileList(m_player->hand()->tiles());
    tiles.append(GraphicsTile::fromTileList(
            m_player->hand()->discardedTiles()));

    foreach (GraphicsTile *tile, tiles)
    {
        tile->setAcceptHoverEvents(false);
        disconnect(tile, 0, this, 0);
    }
}

void GraphicsPlayer::on_tile_selected(Tile *tile)
{
    m_player->discard(tile);
}
