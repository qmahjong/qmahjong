#include "gamegraphics.h"

#include "graphicswall.h"
#include "graphicshand.h"
#include "graphicsplayer.h"

#include "Mahjong"

#include <QGraphicsScene>
#include <QTransform>

/*!
  \defgroup GameGraphics Mahjong's Game Graphics
*/

/*!
  \class GameGraphics
  \ingroup GameGraphics

  \brief The GameGraphics class initializes the game's GUI.

  The GameGraphics class has for sole purpose to create and initialize
  the graphical inteface class required to display a \link MahjongGame
  %Mahjong game\endlink onto a QGraphicsScene.

  \sa GraphicsTile GraphicsWall GraphicsHand GraphicsPlayer
*/

/*!
  Construct a GameGraphics with the QGraphicsScene \a scene as its parent
  and then initialize and position the GUI elements which will be used to
  display the MahjongGame \a game onto the QGraphicsScene \a scene.
*/
GameGraphics::GameGraphics(MahjongGame *game, QGraphicsScene *scene) :
        QObject(scene)
{
    setUpGame(game);
}

/*!
  Destroy the GameGraphics.
*/
GameGraphics::~GameGraphics()
{
}

/*!
  Returns the QGraphicsScene which was used to make this GameGraphics.
*/
QGraphicsScene* GameGraphics::scene() const
{
    return qobject_cast<QGraphicsScene*>(parent());
}

/*!
  \internal

  This function initializes and positions the GUI elements which will
  be used to display the MahjongGame \a game onto a QGraphicsScene.
*/
void GameGraphics::setUpGame(MahjongGame *game)
{
    QList<Player*> players = game->players();
    Player *human = players.at(0);
    foreach (Player *player, players)
    {
        if (player->type() == Player::Human)
        {
            human = player;
            break;
        }
    }

    GraphicsWall *wall = new GraphicsWall(game->playingMat()->wall());
    scene()->addItem(wall);

    foreach (Player *player, players)
    {
        int a;
        if (player == human)
            a = 0;
        else if (player->isLeftOf(human))
            a = 1;
        else if (player->isRightOf(human))
            a = 3;
        else
            a = 2;

        QTransform pmat;
        pmat.rotate(90 * a);
        pmat.translate(-280, 360);

        GraphicsPlayer *gPlayer = new GraphicsPlayer(player);
        gPlayer->setPos(pmat.dx(), pmat.dy());
        scene()->addItem(gPlayer);

        QTransform hmat;
        hmat.rotate(90 * a);
        hmat.translate(0, 360);

        GraphicsHand *hand = new GraphicsHand(player->hand());
        hand->setPos(hmat.dx(), hmat.dy());
        hand->setRotation(90 * a);
        scene()->addItem(hand);
    }
}
