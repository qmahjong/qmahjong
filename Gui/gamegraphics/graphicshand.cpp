#include "graphicshand.h"
#include "graphicstile.h"

#include "Mahjong"

#include <QGraphicsRectItem>
#include <QPainter>
#include <QPropertyAnimation>
#include <QSequentialAnimationGroup>
#include <QParallelAnimationGroup>

/*!
  \class GraphicsHand
  \ingroup GameGraphics

  \brief The GraphicsHand class is the graphical representation of a
  \link Hand %Mahjong hand\endlink.

  \sa GameGraphics
*/

/*!
  Construct a GraphicsHand which doesn't represent a %Mahjong hand.
  This is usefull for implementing \b NULL objects.
*/
GraphicsHand::GraphicsHand(QGraphicsItem *parent) :
        QGraphicsObject(parent)
{
}

/*!
  Construct a GraphicsHand which represents the hand \a hand.
*/
GraphicsHand::GraphicsHand(Hand *hand, QGraphicsItem *parent) :
        QGraphicsObject(parent),
        m_hand(hand)
{
    QGraphicsRectItem *rect = new QGraphicsRectItem(this);
    rect->setRect(-240, -30, 480, 60);
    rect->setBrush(QBrush(QColor::fromRgbF(0.5, 1.0, 0.5, 0.5)));

    connect(hand, SIGNAL(changed()), SLOT(on_hand_changed()));

    hand->player()->setProperty("graphicHand",
                                QVariant::fromValue<QObject*>(this));
}

/*!
  Destroy the GraphicsHand
*/
GraphicsHand::~GraphicsHand()
{
}

QRectF GraphicsHand::boundingRect() const
{
    return childrenBoundingRect();
}

void GraphicsHand::paint(QPainter * /*painter*/,
                         const QStyleOptionGraphicsItem * /*option*/,
                         QWidget * /*widget*/)
{
}

void GraphicsHand::placeOpenTiles(QSequentialAnimationGroup * /*animations*/)
{
}

void GraphicsHand::placeDiscardTiles(QSequentialAnimationGroup *animations)
{
    QParallelAnimationGroup *anims = new QParallelAnimationGroup();

    QList<GraphicsTile*> discardTiles =
            GraphicsTile::fromTileList(m_hand->discardedTiles());

    int tileCount = discardTiles.count();
    QSize tileSize(GraphicsTile::staticSize());
    float xOffset, yOffset = -6.5 * tileSize.height();

    for (int i = 0; i < tileCount; ++i)
    {
        // find what the tile's position should be
        if (i % 6)
        {
            xOffset += tileSize.width();
        }
        else
        {
            xOffset = -6 * tileSize.width()/2.0f + tileSize.width()/2.0f;
            yOffset += tileSize.height();
        }
        QPointF tilePos(xOffset, yOffset);

        // animate the tile if needed
        if (discardTiles.at(i)->pos() != tilePos)
        {
            QPropertyAnimation *posAnim =
                    new QPropertyAnimation(discardTiles.at(i), "pos");
            posAnim->setEndValue(tilePos);
            posAnim->setDuration(1000);

            anims->addAnimation(posAnim);

            if (i+1 == tileCount)
            {
                disconnect(animations, SIGNAL(finished()),
                           m_hand->game(), SLOT(unlockUpdate()));
                connect(anims, SIGNAL(finished()),
                        m_hand->game(), SLOT(unlockUpdate()));
            }
        }
    }

    animations->addAnimation(anims);
}

void GraphicsHand::placeClosedTiles(QSequentialAnimationGroup *animations)
{
    QParallelAnimationGroup *anims = new QParallelAnimationGroup();

    QList<GraphicsTile*> closedTiles =
            GraphicsTile::fromTileList(m_hand->closedTiles());

    int tileCount = closedTiles.count();
    int tileWidth = GraphicsTile::staticSize().width();
    float xOffset = -tileCount * tileWidth/2.0f + tileWidth/2.0f;

    for (int i = 0; i < tileCount; ++i, xOffset += tileWidth)
    {
        GraphicsTile *tile = closedTiles[i];

        if (tile->parentItem() != this)
        {
            tile->setZValue(2);

            QPointF mappedPos = mapFromScene(tile->scenePos());
            tile->setRotation(tile->rotation() - rotation());
            if (tile->rotation() > 180)
                tile->setRotation(tile->rotation() - 360);
            else if (tile->rotation() < -180)
                tile->setRotation(tile->rotation() + 360);
            tile->setParentItem(this);
            tile->setPos(mappedPos);

            QPropertyAnimation *rotAnim =
                    new QPropertyAnimation(tile, "rotation");
            rotAnim->setEndValue(0);
            rotAnim->setDuration(1000);

            anims->addAnimation(rotAnim);
            if (m_hand->player()->isLocalHuman())
                connect(anims, SIGNAL(finished()), tile, SLOT(flip()));
        }

        QPropertyAnimation *posAnim = new QPropertyAnimation(tile, "pos");
        posAnim->setEndValue(QPointF(xOffset, 0));
        posAnim->setDuration(1000);

        anims->addAnimation(posAnim);
    }

    animations->addAnimation(anims);
}

void GraphicsHand::on_hand_changed()
{
    m_hand->game()->lockUpdate();

    QSequentialAnimationGroup *anims = new QSequentialAnimationGroup(this);
    connect(anims, SIGNAL(finished()), m_hand->game(), SLOT(unlockUpdate()));

    placeOpenTiles(anims);
    placeDiscardTiles(anims);
    placeClosedTiles(anims);

    anims->start(QAbstractAnimation::DeleteWhenStopped);
}
