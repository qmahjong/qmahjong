#include "graphicswall.h"

#include "graphicstile.h"

#include "Mahjong"

#include <QtGlobal>
#include <QVariant>
#include <QList>
#include <QTransform>
#include <QPainter>
#include <QPropertyAnimation>
#include <QSequentialAnimationGroup>
#include <QParallelAnimationGroup>

/*!
  \class GraphicsWall
  \ingroup GameGraphics

  \brief The GraphicsWall calls is a graphics representation of a
  \link Wall %Mahjong wall\endlink.

  \sa GameGraphics
*/

/*!
  Construct a GraphicsWall which does not represent a wall.
  This is useful for implementing \b NULL objects.
*/
GraphicsWall::GraphicsWall(QGraphicsItem *parent) :
        QGraphicsObject(parent),
        m_wall(0)
{
}

/*!
  Construct a GraphicsWall which represents the wall \a wall.
*/
GraphicsWall::GraphicsWall(Wall *wall, QGraphicsItem *parent) :
        QGraphicsObject(parent),
        m_wall(wall)
{
    connect(wall, SIGNAL(shuffled()), SLOT(on_wall_shuffled()));
    connect(wall, SIGNAL(broken()), SLOT(on_wall_broken()));
    connect(wall, SIGNAL(deadWallChanged()), SLOT(on_wall_deadWallChanged()));
}

/*!
  Destroy the GraphicsWall.
*/
GraphicsWall::~GraphicsWall()
{
}

QRectF GraphicsWall::boundingRect() const
{
    return QRectF(-400, -400, 800, 800);
}

void GraphicsWall::paint(QPainter * /*painter*/,
                         const QStyleOptionGraphicsItem *,
                         QWidget *)
{
    /*
    painter->drawRect(QRectF(-320, -320, 640, 640));
    QRect r(0, 0, 320, 320);
    painter->drawRect(r.translated(-320, -320));
    painter->drawRect(r);
    */
}

void GraphicsWall::on_wall_shuffled()
{
    m_wall->game()->lockUpdate();

    QList<GraphicsTile*> tiles(GraphicsTile::fromTileList(m_wall->tiles(), this));

    QList<GraphicsTile*>::const_iterator tileIt = tiles.begin();
    const int rowSize = tiles.count()/8;
    const int wallRadius = (tiles.count() + 4) * 2;
    const int tileWidth = GraphicsTile::staticSize().width();
    const int tileHeight = GraphicsTile::staticSize().height();

    QSequentialAnimationGroup *anim0 = new QSequentialAnimationGroup();
    QParallelAnimationGroup *anim1[5];
    const int anim1Count = static_cast<int>(sizeof(anim1)/sizeof(anim1[0]));
    for (int i = 0; i < anim1Count; ++i)
    {
        anim1[i] = new QParallelAnimationGroup();
    }
    QParallelAnimationGroup *anim2 = new QParallelAnimationGroup();

    for (int r = 0; r < 8; ++r)
    {
        for (int c = 0; c < rowSize; ++c)
        {
            GraphicsTile *gTile = *tileIt++;
            // give the initial position
            QPropertyAnimation *pos0 = new QPropertyAnimation(gTile, "pos");
            pos0->setEndValue(QPointF(tileWidth * (c - rowSize/2.0 + 0.5),
                                      tileHeight * (r - 3.5)));
            pos0->setDuration(50);
            anim0->addAnimation(pos0);

            // create the shuffle animation
            for (int i = 0; i < anim1Count; ++i)
            {
                QPropertyAnimation *pos1 = new QPropertyAnimation(gTile, "pos");
                pos1->setEndValue(QPointF((qrand()%600) - 300,
                                          (qrand()%600) - 300));
                pos1->setDuration(800);

                QPropertyAnimation *rot1 = new QPropertyAnimation(gTile, "rotation");
                rot1->setEndValue(qrand()%360);
                rot1->setDuration(800);

                QParallelAnimationGroup *mov1 = new QParallelAnimationGroup();
                mov1->addAnimation(pos1);
                mov1->addAnimation(rot1);
                anim1[i]->addAnimation(mov1);
            }
        }
    }

    // give tiles their final position
    int humanAngle = 0;
    foreach (Player *p, m_wall->game()->players())
    {
        if (p->type() == Player::Human)
            break;
        else
            ++humanAngle;
    }
    tileIt = tiles.begin();
    for (int r = 0; r < 4; ++r)
    {
        int a = (r + humanAngle) % 4;
        for (int i = 0; i < rowSize; ++i)
        {
            GraphicsTile *gTile = *tileIt++;
            gTile->setZValue(1.5);

            QPointF pos;
            switch (a)
            {
            case 0:
                pos = QPointF(tileWidth * (-i + rowSize/2.0 - 0.5), wallRadius);
                break;

            case 1:
                pos = QPointF(-wallRadius, tileWidth * (-i + rowSize/2.0 - 0.5));
                break;

            case 2:
                pos = QPointF(tileWidth * (i - rowSize/2.0 + 0.5), -wallRadius);
                break;

            case 3:
                pos = QPointF(wallRadius, tileWidth * (i - rowSize/2.0 + 0.5));
                break;
            }

            QPropertyAnimation *pos2 = new QPropertyAnimation(gTile, "pos");
            pos2->setEndValue(pos);
            pos2->setDuration(800);

            QPropertyAnimation *rot2 = new QPropertyAnimation(gTile, "rotation");
            rot2->setEndValue(90 * a);
            rot2->setDuration(800);

            QParallelAnimationGroup *mov2 = new QParallelAnimationGroup();
            mov2->addAnimation(pos2);
            mov2->addAnimation(rot2);
            anim2->addAnimation(mov2);

            gTile = *tileIt++;
            gTile->setZValue(1);

            switch(a)
            {
            case 0:
                pos += QPointF(0, 5);
                break;

            case 1:
                pos += QPointF(5, 5);
                break;

            case 2:
                pos += QPointF(0, 5);
                break;

            case 3:
                pos += QPointF(-5, 5);
                break;
            }

            pos2 = new QPropertyAnimation(gTile, "pos");
            pos2->setEndValue(pos);
            pos2->setDuration(800);

            rot2 = new QPropertyAnimation(gTile, "rotation");
            rot2->setEndValue(90 * a);
            rot2->setDuration(800);

            mov2 = new QParallelAnimationGroup();
            mov2->addAnimation(pos2);
            mov2->addAnimation(rot2);
            anim2->addAnimation(mov2);
        }
    }

    QSequentialAnimationGroup *animRoot = new QSequentialAnimationGroup(this);
    animRoot->addAnimation(anim0);
    animRoot->addPause(500);
    for (int i = 0; i < anim1Count; ++i)
    {
        animRoot->addAnimation(anim1[i]);
    }
    animRoot->addAnimation(anim2);
    animRoot->addPause(500);

    connect(animRoot, SIGNAL(finished()), m_wall->game(), SLOT(unlockUpdate()));
    animRoot->start(QAbstractAnimation::DeleteWhenStopped);
}

void GraphicsWall::on_wall_broken()
{
    m_wall->game()->lockUpdate();

    QList<GraphicsTile*> tiles = GraphicsTile::fromTileList(m_wall->tiles());

    if (tiles.first()->rotation() != tiles.last()->rotation())
    {
        m_wall->game()->unlockUpdate();
        return;
    }

    QParallelAnimationGroup *anims = new QParallelAnimationGroup(this);
    qreal rotation = tiles.first()->rotation();
    QPointF offset = QTransform()
                     .rotate(rotation)
                     .translate(5, 0)
                     .map(QPointF());
    QList<GraphicsTile*>::iterator it;
    GraphicsTile *tile;

    it = tiles.begin();
    for (tile = *it; tile->rotation() == rotation; tile = *(++it))
    {
        QPropertyAnimation *posAnim = new QPropertyAnimation(tile, "pos");
        posAnim->setEndValue(tile->pos() - offset);
        posAnim->setDuration(500);

        anims->addAnimation(posAnim);
    }

    it = --tiles.end();
    for (tile = *it; tile->rotation() == rotation; tile = *(--it))
    {
        QPropertyAnimation *posAnim = new QPropertyAnimation(tile, "pos");
        posAnim->setEndValue(tile->pos() + offset);
        posAnim->setDuration(500);

        anims->addAnimation(posAnim);
    }

    connect(anims, SIGNAL(finished()), m_wall->game(), SLOT(unlockUpdate()));
    anims->start(QAbstractAnimation::DeleteWhenStopped);
}

void GraphicsWall::on_wall_deadWallChanged()
{
    m_wall->game()->lockUpdate();

    QParallelAnimationGroup *anims = new QParallelAnimationGroup(this);
    QList<GraphicsTile*> tiles = GraphicsTile::fromTileList(m_wall->deadWall());

    int cut = 0;
    const int count = tiles.count();
    qreal rot = tiles[0]->rotation();

    // find if there is a corner cut in the dead wall
    for (int i = 1; i < count; ++i)
    {
        if (tiles[i]->rotation() == rot)
            ++cut;
        else
            break;
    }

    // aling the tiles with the biggest part
    if (count/2 < cut)
    {
        QPointF diff = tiles.at(0)->pos() - tiles.at(2)->pos();
        QPointF offset = QTransform()
                         .rotate(rot)
                         .translate(3, -1)
                         .map(QPointF());

        for (int i = 0; i < count; ++i)
        {
            GraphicsTile *tile = tiles[i];
            QPropertyAnimation *posAnim = new QPropertyAnimation(tile, "pos");
            posAnim->setDuration(500);

            if (i <= cut)
                posAnim->setEndValue(tile->pos() - offset);
            else
            {
                posAnim->setEndValue(tiles.at(i%2)->pos()
                                     - diff * ((i - 1 + i%2)/2)
                                     - offset);

                QPropertyAnimation *rotAnim = new QPropertyAnimation(tile, "rotation");
                rotAnim->setDuration(500);
                rotAnim->setEndValue(rot);

                anims->addAnimation(rotAnim);
            }

            anims->addAnimation(posAnim);
        }
    }
    else
    {
        rot = tiles.last()->rotation();

        QPointF diff = tiles.at(count-1)->pos() - tiles.at(count-3)->pos();
        QPointF offset = QTransform()
                         .rotate(rot)
                         .translate(3, -1)
                         .map(QPointF());

        for (int i = count-1; i >= 0; --i)
        {
            GraphicsTile *tile = tiles[i];
            QPropertyAnimation *posAnim = new QPropertyAnimation(tile, "pos");
            posAnim->setDuration(500);

            if (i > cut)
                posAnim->setEndValue(tile->pos() - offset);
            else
            {
                posAnim->setEndValue(tiles.at(count - 2 + i%2)->pos()
                                     - diff * ((count - 1 - i + i%2)/2)
                                     - offset);

                QPropertyAnimation *rotAnim = new QPropertyAnimation(tile, "rotation");
                rotAnim->setDuration(500);
                rotAnim->setEndValue(rot);

                anims->addAnimation(rotAnim);
            }

            anims->addAnimation(posAnim);
        }
    }

    connect(anims, SIGNAL(finished()), m_wall->game(), SLOT(unlockUpdate()));
    anims->start(QAbstractAnimation::DeleteWhenStopped);
}
