#ifndef GRAPHICSPLAYER_H
#define GRAPHICSPLAYER_H

#include <QGraphicsObject>

class Player;
class Tile;

class GraphicsPlayer : public QGraphicsObject
{
    Q_OBJECT
    Q_PROPERTY(QString playerName READ playerName WRITE setPlayerName)

public:
    explicit GraphicsPlayer(QGraphicsItem *parent = 0);
    explicit GraphicsPlayer(Player *player, QGraphicsItem *parent = 0);
    virtual ~GraphicsPlayer();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter,
                       const QStyleOptionGraphicsItem *option,
                       QWidget *widget);

    QString playerName() const; /*!< \sa playerName */

public slots:
    void setPlayerName(const QString &name);

private:
    QGraphicsTextItem *m_name;
    Player *m_player;

private slots:
    void on_player_drawEntered();
    void on_player_discardEntered();
    void on_player_discardExited();
    void on_tile_selected(Tile *tile);
};

#endif // GRAPHICSPLAYER_H
