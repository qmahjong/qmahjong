#include "graphicstile.h"

#include "Mahjong"

#include <QPainter>
#include <QGraphicsTextItem>
#include <QPropertyAnimation>
#include <QGraphicsEffect>

/*!
  \class GraphicsTile
  \ingroup GameGraphics

  \brief The GraphicsTile class is the graphical representation of a Tile.

  \sa GameGraphics
*/

/*!
  \property GraphicsTile::faceUp

  This property holds whether this %GraphicsTile is displayed face up
  or face down on the screen.
*/

/*!
  \property GraphicsTile::flipAngle

  This property holds the angle at which this tile is diplayed on the screen.
*/

/*!
  \fn GraphicsTile::selected(Tile *tile)

  This signal is emitted when the tile is selected by the mouse. The tile
  this %GraphicsTile represents is passed in the \a tile parameter.
*/

/*!
  Construct a GraphicsTile which doesn't represent a tile.
  This is useful for implementing \b NULL objects.
*/
GraphicsTile::GraphicsTile(QGraphicsItem *parent) :
        QGraphicsObject(parent),
        m_text(new QGraphicsTextItem(this)),
        m_flipAngle(0),
        m_tile(0)
{
    setAcceptedMouseButtons(Qt::LeftButton);
    m_text->hide();
    setText("<span style='color:blue;'>MJ</span>");
}

/*!
  Construct a GraphicsTile which is represents the tile \a tile.
*/
GraphicsTile::GraphicsTile(Tile *tile, QGraphicsItem *parent) :
        QGraphicsObject(parent),
        m_text(new QGraphicsTextItem(this)),
        m_flipAngle(0),
        m_tile(tile)
{
    setAcceptedMouseButtons(Qt::LeftButton);
    m_text->hide();
    setText(tile->shortText(true));
    connect(tile, SIGNAL(flipped(Tile::Facing)), SLOT(on_tile_flipped(Tile::Facing)));
}

/*!
  Destroy the GraphicsTile.
*/
GraphicsTile::~GraphicsTile()
{
}

QRectF GraphicsTile::boundingRect() const
{
    return QRectF(QPointF(-15, -22.5), staticSize());
}

void GraphicsTile::paint(QPainter *painter,
                       const QStyleOptionGraphicsItem * /*option*/,
                       QWidget * /*widget*/)
{
    painter->save();

    if (graphicsEffect())
        painter->setPen(QColor::fromRgbF(0.2, 0.6, 1, 0.8));
    else
        painter->setPen(Qt::NoPen);

    if (isFaceUp())
    {
        QRadialGradient gradient(0, 0, 45, -9, -16);
        gradient.setColorAt(0, Qt::white);
        gradient.setColorAt(0.05, QColor::fromRgbF(0.85, 0.9, 0.9));
        gradient.setColorAt(0.5, QColor::fromRgbF(0.85, 0.85, 0.8));
        gradient.setColorAt(0.75, QColor::fromRgbF(0.65, 0.7, 0.65));
        gradient.setColorAt(0.8, QColor::fromRgbF(0.3, 0.3, 0.3));
        gradient.setColorAt(0.9, Qt::black);
        painter->setBrush(QBrush(gradient));
    }
    else
    {
        QRadialGradient gradient(0, 0, 45, -9, -16);
        gradient.setColorAt(0, Qt::white);
        gradient.setColorAt(0.05, QColor::fromRgbF(1, 0.85, 0.2));
        gradient.setColorAt(0.5, QColor::fromRgbF(1, 0.65, 0));
        gradient.setColorAt(0.75, QColor::fromRgbF(0.65, 0.45, 0));
        gradient.setColorAt(0.8, QColor::fromRgbF(0.2, 0.05, 0));
        gradient.setColorAt(0.9, Qt::black);
        painter->setBrush(QBrush(gradient));
    }

    painter->drawRoundedRect(boundingRect(), 5, 5);

    painter->restore();
}

/*!
  Returns \c true if this tile is face up on the screen.
*/
bool GraphicsTile::isFaceUp() const
{
    return m_flipAngle > 90;
}

/*!
  \return the angle at which this tile is displayed on the screen.

  \sa setFlipAngle
*/
int GraphicsTile::flipAngle() const
{
    return m_flipAngle;
}

/*!
  Sets the angle at which this tile displayed on the screen to \a angle.

  \sa flipAngle
*/
void GraphicsTile::setFlipAngle(int angle)
{
    if (angle == m_flipAngle)
        return;
    if (angle >= 90 && m_flipAngle < 90)
    {
        m_text->show();
        if(m_tile)
            setToolTip(m_tile->toString());
    }
    else if (angle < 90 && m_flipAngle >= 90)
    {
        m_text->hide();
        setToolTip(QString());
    }

    m_flipAngle = angle;

    angle += isFaceUp() ? 180 : 0;
    setTransform(QTransform().rotate(rotation(), Qt::ZAxis)
                             .rotate(angle, Qt::XAxis)
                             .rotate(-rotation(), Qt::ZAxis));
}

/*!
  Flip this tile to display its other side.
*/
void GraphicsTile::flip()
{
    QPropertyAnimation *anim = new QPropertyAnimation(this, "flipAngle", this);
    anim->setEndValue(isFaceUp() ? 0 : 180);
    anim->setDuration(1000);
    anim->start(QAbstractAnimation::DeleteWhenStopped);
}

void GraphicsTile::hoverEnterEvent(QGraphicsSceneHoverEvent *)
{
    setZValue(2.5);

    QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect();
    effect->setOffset(0);
    effect->setBlurRadius(10);
    effect->setColor(QColor::fromRgbF(0.2, 0.6, 1, 1));
    setGraphicsEffect(effect);
    update();
}

void GraphicsTile::hoverLeaveEvent(QGraphicsSceneHoverEvent *)
{
    setZValue(2);

    setGraphicsEffect(0);
    update();
}

void GraphicsTile::mousePressEvent(QGraphicsSceneMouseEvent *e)
{
    QGraphicsObject::mouseReleaseEvent(e);
}

void GraphicsTile::mouseReleaseEvent(QGraphicsSceneMouseEvent *e)
{
    setGraphicsEffect(0);
    emit selected(m_tile);
    QGraphicsObject::mouseReleaseEvent(e);
}

/*!
  Sets the text that this tile displays to \a text.
*/
void GraphicsTile::setText(const QString &text)
{
    m_text->setHtml(text);
    m_text->setPos(-m_text->boundingRect().center());
    m_text->setTransformOriginPoint(m_text->boundingRect().center());
}

void GraphicsTile::on_tile_flipped(Tile::Facing faceUp)
{
    if (faceUp == isFaceUp())
        return;

    m_tile->game()->lockUpdate();

    QPropertyAnimation *anim = new QPropertyAnimation(this, "flipAngle", this);
    anim->setEndValue(faceUp ? 180 : 0);
    anim->setDuration(1000);
    connect(anim, SIGNAL(finished()), m_tile->game(), SLOT(unlockUpdate()));
    anim->start(QAbstractAnimation::DeleteWhenStopped);
}

/*!
  Returns the static size of a GraphicsTile.
*/
QSize GraphicsTile::staticSize()
{
    return QSize(30, 45);
}

/*!
  Returns the %GraphicsTile which represents the tile \a tile. If the
  the given tile is not represented by a %GraphicsTile, then a new one
  is made with \a parent as its parent.

  \sa fromTileList
*/
GraphicsTile* GraphicsTile::fromTile(Tile *tile, QGraphicsItem *parent)
{
    GraphicsTile *gTile =
            qobject_cast<GraphicsTile*>(tile->property("graphicsTile")
                                                       .value<QObject*>());

    if (!gTile)
    {
        gTile = new GraphicsTile(tile, parent);
        tile->setProperty("graphicsTile",
                          QVariant::fromValue<QObject*>(gTile));
    }

    return gTile;
}

/*!
  Returns a list of %GraphicsTile which represents the list of tile given
  by \a tiles. If a tile is not represented by a %GraphicsTile, then a
  new one is made with \a parent as its parent.

  \sa fromTile
*/
QList<GraphicsTile*> GraphicsTile::fromTileList(const TileList &tiles,
                                                QGraphicsItem *parent)
{
    QList<GraphicsTile*> gTiles;

    foreach (Tile *tile, tiles)
    {
        GraphicsTile *gTile =
                qobject_cast<GraphicsTile*>(tile->property("graphicsTile")
                                                           .value<QObject*>());

        if (!gTile)
        {
            gTile = new GraphicsTile(tile, parent);
            tile->setProperty("graphicsTile",
                              QVariant::fromValue<QObject*>(gTile));
        }

        gTiles.push_back(gTile);
    }

    return gTiles;
}
