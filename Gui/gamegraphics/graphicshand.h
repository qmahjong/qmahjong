#ifndef GRAPHICSHAND_H
#define GRAPHICSHAND_H

#include <QGraphicsObject>

class Hand;
class QSequentialAnimationGroup;

class GraphicsHand : public QGraphicsObject
{
    Q_OBJECT

public:
    explicit GraphicsHand(QGraphicsItem *parent = 0);
    explicit GraphicsHand(Hand *hand, QGraphicsItem *parent = 0);
    virtual ~GraphicsHand();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter,
                       const QStyleOptionGraphicsItem *option,
                       QWidget *widget);

private:
    void placeOpenTiles(QSequentialAnimationGroup *animations);
    void placeDiscardTiles(QSequentialAnimationGroup *animations);
    void placeClosedTiles(QSequentialAnimationGroup *animations);

    Hand *m_hand;

private slots:
    void on_hand_changed();
};

#endif // GRAPHICSHAND_H
