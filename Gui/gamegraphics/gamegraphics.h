#ifndef GAMEGRAPHICS_H
#define GAMEGRAPHICS_H

#include <QObject>

class MahjongGame;

class QGraphicsScene;

class GameGraphics : public QObject
{
    Q_OBJECT

public:
    GameGraphics(MahjongGame *game, QGraphicsScene *scene);
    virtual ~GameGraphics();

    QGraphicsScene* scene() const;

private:
    void setUpGame(MahjongGame *game);
};

#endif // GAMEGRAPHICS_H
