#ifndef GRAPHICSWALL_H
#define GRAPHICSWALL_H

#include <QGraphicsObject>

class Wall;

class GraphicsWall : public QGraphicsObject
{
    Q_OBJECT

public:
    explicit GraphicsWall(QGraphicsItem *parent = 0);
    explicit GraphicsWall(Wall *wall, QGraphicsItem *parent = 0);
    virtual ~GraphicsWall();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter,
                       const QStyleOptionGraphicsItem *option,
                       QWidget *widget);

private:
    Wall *m_wall;

private slots:
    void on_wall_shuffled();
    void on_wall_broken();
    void on_wall_deadWallChanged();
};

#endif // GRAPHICSWALL_H
