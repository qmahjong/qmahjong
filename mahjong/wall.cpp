#include "wall.h"

#include "playingmat.h"
#include "mahjonggame.h"

#include <QReadLocker>
#include <QWriteLocker>

/*!
  \class Wall
  \ingroup MahjongCore

  \brief The Wall class represents a wall of %Mahong tile.

  The wall is built at the beginning of each round after which 13 tiles
  from the wall (14 for the dealer) are distributed to each player. Each
  player's turn, a tile is drawn from the wall until only the dead-wall
  is left.

  \sa Tile PlayingMat
*/

/*!
  \property Wall::tiles

  This property holds the tiles of which this wall is made of.
*/

/*!
  \var Wall::m_tiles

  This attribute holds the tiles of which this wall is made of.

  \sa TileList
*/

/*!
  \var Wall::m_deadWallLength

  This attribute holds the length of the dead wall.

  \sa deadWall
*/

/*!
  \fn Wall::shuffled

  This signal is emitted whenever the wall's tiles are shuffled.

  \sa shuffle
*/

/*!
  \fn Wall::broken

  This signal is emitted whenever the wall is broken.

  \sa breakWall
*/

/*!
  \fn Wall::deadWallChanged

  This signal is emitted whenever the size of the dead wall changes.

  \sa setDeadWall
*/

/*!
  \fn Wall::depleted

  This signal is emited when the wall's first tile is requested, but
  there are no more tiles available for draw.

  \sa takeFirst
*/

/*!
  Construct an empty wall. Tiles are added after with Wall::setTiles.
*/
Wall::Wall(QObject *parent) :
        QObject(parent),
        m_deadWallLength(0)
{
}

/*!
  Destroy the wall.
*/
Wall::~Wall()
{
}

/*!
  Returns the \link MahjongGame game\endlink to which this wall belongs.
*/
MahjongGame* Wall::game() const
{
    return qobject_cast<MahjongGame*>(parent()->parent());
}

/*!
  Returns the \link PlayingMat playing mat\endlink on which this wall is.
*/
PlayingMat* Wall::playingMat() const
{
    return qobject_cast<PlayingMat*>(parent());
}

/*!
  \return the tiles of which this wall is made of.

  \sa setTiles
*/
TileList Wall::tiles() const
{
    QReadLocker locker(&m_rwlock);

    return m_tiles;
}

/*!
  Sets the tiles of which this wall is made of to \a tiles.

  \sa tiles
*/
void Wall::setTiles(TileList tiles)
{
    QWriteLocker locker(&m_rwlock);

    m_tiles = tiles;
    m_deadWallLength = 0;
}

/*!
  \return the tiles which make the dead-wall part of this wall.
*/
TileList Wall::deadWall() const
{
    QReadLocker locker(&m_rwlock);

    return m_tiles.mid(m_tiles.count() - m_deadWallLength);
}

/*!
  Returns \c true when there are still tiles available to draw.
*/
bool Wall::canDraw() const
{
    QReadLocker locker(&m_rwlock);

    return m_tiles.count() > m_deadWallLength;
}

/*!
  Returns a tile by removing it from the wall at position \a i.
*/
Tile* Wall::takeAt(int i)
{
    QWriteLocker locker(&m_rwlock);

    return m_tiles.takeAt(i);
}

/*!
  Returns a tile by removing the first available tile from the wall.
  If there are no more tiles available for draw, a \b NULL pointer is
  returned and the signal Wall::depleted is emitted.

  \sa canDraw
*/
Tile* Wall::takeFirst()
{
    QWriteLocker locker(&m_rwlock);

    if (m_tiles.count() > m_deadWallLength)
        return m_tiles.takeFirst();

    emit depleted();
    return NULL;
}

/*!
  Returns a tile by removing that last tile from the wall.

  \sa takeFirst
*/
Tile* Wall::takeLast()
{
    QWriteLocker locker(&m_rwlock);

    return m_tiles.takeLast();
}

/*!
  Shuffles the tiles of which this wall is made of.

  This function emits the signal Wall::shuffled.
*/
void Wall::shuffle()
{
    QWriteLocker locker(&m_rwlock);

    m_tiles.shuffle();

    locker.unlock();
    emit shuffled();
    game()->update();
}

/*!
  Break the wall by making the tile at position \a position the new first
  tile. The tile which was at position \a position - 1 is now the last
  tile in the wall.

  This function emits the signal Wall::broken.
*/
void Wall::breakWall(int position)
{
    QWriteLocker locker(&m_rwlock);

    m_tiles = m_tiles.mid(position) << m_tiles.mid(0, position);

    locker.unlock();
    emit broken();
    game()->update();
}

/*!
  Sets the dead wall to be the \a n last tiles in the wall.

  This function emits the signal Wall::deadWallChanged.
*/
void Wall::setDeadWall(int n)
{
    QWriteLocker locker(&m_rwlock);

    m_deadWallLength = n;

    locker.unlock();
    emit deadWallChanged();
    game()->update();
}
