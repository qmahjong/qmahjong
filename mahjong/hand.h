#ifndef MAHJONG_HAND_H
#define MAHJONG_HAND_H

#include "mahjong_global.h"
#include "tilelist.h"

#include <QObject>
#include <QMetaType>
#include <QString>
#include <QMultiHash>
#include <QReadWriteLock>

class MahjongGame;
class Player;

class MAHJONGSHARED_EXPORT Hand : public QObject
{
    Q_OBJECT
    Q_PROPERTY(TileList tiles READ tiles)
    Q_PROPERTY(TileList closedTiles READ closedTiles)
    Q_PROPERTY(TileList openedTiles READ openedTiles)
    Q_PROPERTY(TileList discardedTiles READ discardedTiles)
    Q_PROPERTY(Tile* riichiTile READ riichiTile WRITE setRiichiTile)

public:
    explicit Hand(QObject *parent = 0);
    virtual ~Hand();

    virtual MahjongGame* game() const;

    virtual Player* player() const;

    TileList tiles() const; /*!< \sa tiles */
    TileList closedTiles() const; /*!< \sa closedTiles */
    TileList openedTiles() const; /*!< \sa openedTiles */
    TileList discardedTiles() const; /*!< \sa discardedTiles */

    Q_INVOKABLE bool isOpened() const;
    Q_INVOKABLE bool isRiichi() const;

    Tile* riichiTile() const; /*!< \sa riichiTile */
    void setRiichiTile(Tile *tile);

    Q_INVOKABLE virtual QString toString() const;

    /*! QString type converter. */
    operator QString() const { return toString(); }

public slots:
    void giveTiles(TileList tiles);
    void drawTile(Tile *tile);
    void discardTile(Tile *tile);

    void sortTiles();

signals:
    void changed();

protected:
    /*! Read/Write lock for thread synchronisation. */
    mutable QReadWriteLock m_rwlock;

private:
    /*! The hand's closed tiles. */
    TileList m_closed;
    /*! The hand's opened tiles. */
    TileList m_opened;
    /*! The hand's discarded tiles. */
    TileList m_discarded;

    /*! The tile which was discarded when riichi was declared. */
    Tile *m_riichiTile;
};

Q_DECLARE_METATYPE(Hand*)

#endif // MAHJONG_HAND_H
