#ifndef MAHJONG_TILE_H
#define MAHJONG_TILE_H

#include "mahjong_global.h"

#include <QObject>
#include <QMetaType>
#include <QString>
#include <QReadWriteLock>

class TileList;
class MahjongGame;

class MAHJONGSHARED_EXPORT Tile : public QObject
{
    friend class TileList;

    Q_OBJECT
    Q_PROPERTY(Category category READ category WRITE setCategory)
    Q_PROPERTY(Variety variety READ variety WRITE setVariety)
    Q_PROPERTY(int value READ value)
    Q_PROPERTY(QString text READ toString)
    Q_PROPERTY(QString shortText READ shortText)
    Q_ENUMS(Category)
    Q_ENUMS(Variety)
    Q_ENUMS(Facing)

public:
    enum Category
    {
        Circle = 0x0100, /*!< Circle tiles. */
        Bamboo = 0x0200, /*!< Bamboo tiles. */
        Character = 0x0300, /*!< Character tiles. */
        Wind = 0x0400, /*!< Wind (honor) tiles. */
        Dragon = 0x0500, /*!< Dragon (honor) tiles. */
        Season = 0x0600, /*!< Season (bonus) tiles. */
        Flower = 0x0700, /*!< Flower (bonus) tiles. */
        CATEGORY_MASK = 0xff00 /*!< Category bit mask. */
    };

    enum Variety
    {
        One = 0x01,
        Two = 0x02,
        Three = 0x03,
        Four = 0x04,
        Five = 0x05,
        Six = 0x06,
        Seven = 0x07,
        Height = 0x08,
        Nine = 0x09,
        East = 0x11, /*!< East wind, Spring/Fisherman season, and Plum flower. */
        South = 0x12, /*!< South wind, Summer/Woodcutter season, and Orchid flower. */
        West = 0x13, /*!< West wind, Autumn/Farmer season, and Chrysanthemum flower. */
        North = 0x14, /*!< North wind, Winter/Scholar season, and Bamboo flower. */
        White = 0x21, /*!< White dragon. */
        Green = 0x22, /*!< Green dragon. */
        Red = 0x23, /*!< Red dragon. */
        VARIETY_MASK = 0xff /*!< Variety bit mask. */
    };

    enum Facing
    {
        FaceDown, /*!< The tile's face is hidden. */
        FaceUp /*!< The tile's face is visible. */
    };

    typedef Tile* (*factory)(Category, Variety, QObject*);

    Tile(Category category, Variety variety, QObject *parent = 0);
    virtual ~Tile();

    virtual MahjongGame* game() const;

    Category category() const; /*!< \sa category */
    void setCategory(Category category);
    Variety variety() const; /*!< \sa variety */
    void setVariety(Variety variety);
    int value() const; /*!< \sa value */

    bool isFaceUp() const;
    virtual bool isHonor() const;
    virtual bool isTerminal() const;
    virtual bool isBonus() const;

    virtual QString categoryText() const;
    virtual QString shortCategoryText() const;
    virtual QString varietyText() const;
    virtual QString shortVarietyText() const;

    Q_INVOKABLE virtual QString toString(bool colorize = false) const;
    virtual QString shortText(bool colorize = false) const;

    /*! Overloaded == comparison operator. */
    bool operator==(const Tile &other) const
    { return value() == other.value(); }
    /*! Overloaded > comparison operator. */
    bool operator>(const Tile &other) const
    { return value() > other.value(); }
    /*! Overloaded < comparison operator. */
    bool operator<(const Tile &other) const
    { return value() < other.value(); }

    /*! QString type converter */
    operator QString() const { return toString(); }

public slots:
    void flip(Tile::Facing face = FaceUp);

signals:
    void flipped(Tile::Facing face);

protected:
    virtual QString colorizeText() const;

    /*! Used for sorting. */
    static bool lessThan(const Tile *lhs, const Tile *rhs)
    { return *lhs < *rhs; }
    /*! Used for shuffling. */
    static bool randomLessThan(const Tile *lhs, const Tile *rhs);

    /*! Read/Write lock for thread synchronisation. */
    mutable QReadWriteLock m_rwlock;

    Category m_category;
    Variety m_variety;
    Facing m_facing;
};

Q_DECLARE_METATYPE(Tile*)

#endif // MAHJONG_TILE_H
