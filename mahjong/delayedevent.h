#ifndef DELAYEDEVENT_H
#define DELAYEDEVENT_H

#include <QObject>
#include <QStateMachine>

class DelayedEvent : public QObject
{
    Q_OBJECT

public:
    DelayedEvent(QEvent *event, QStateMachine *machine, QObject *parent = 0);

public slots:
    void activate();

private:
    QEvent *m_event;
    QStateMachine *m_machine;
};

#endif // DELAYEDEVENT_H
