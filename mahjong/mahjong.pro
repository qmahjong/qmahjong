# -------------------------------------------------
# Project created by QtCreator 2009-12-14T11:46:05
# -------------------------------------------------
TARGET = mahjong
DESTDIR = ../
TEMPLATE = lib
QT += script
CONFIG += shared
include(../qmahjong.pri)
DEFINES += MAHJONG_LIBRARY
SOURCES += mahjong.cpp \
    tile.cpp \
    tilelist.cpp \
    hand.cpp \
    player.cpp \
    wall.cpp \
    playingmat.cpp \
    action.cpp \
    mahjonggame.cpp
HEADERS += mahjong_global.h \
    mahjong.h \
    tile.h \
    tilelist.h \
    hand.h \
    player.h \
    wall.h \
    playingmat.h \
    mahjongrulesinterface.h \
    action.h \
    mahjonggame.h \
    delayedevent.h
HEADERS += ../include/Mahjong
