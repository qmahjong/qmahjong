#ifndef MAHJONG_MAHJONGRULESINTERFACE_H
#define MAHJONG_MAHJONGRULESINTERFACE_H

#include "mahjong.h"

#include <QString>
#include <QVariant>

class QObject;
class QWidget;

class MahjongGame;

/*!
  Abstract class used as the base for all sets of Mahjong rules.
  It is declared as an interface for developing plugins.
*/
class MahjongRulesInterface
{
public:
    /*! Default destructor. */
    virtual ~MahjongRulesInterface() {}

    /*!
      \return The Mahjong rules' name.
    */
    virtual QString rulesName() const = 0;
    /*!
      \return A short name used for sorting and with the settings page.
    */
    virtual QString rulesShortName() const = 0;

    /*!
      \return A description of the rules.
    */
    virtual QString rulesDesc() const = 0;

    /*!
      \return The queried widget.
    */
    virtual QWidget* getWidget(Mahjong::RuleWidget ruleWidget) const = 0;

    /*!
      \return Specialized game for the rules.
    */
    virtual MahjongGame* getGame() = 0;
};

Q_DECLARE_INTERFACE(MahjongRulesInterface, "org.QMahjong.Plugin.Rules");

#endif // MAHJONG_MAHJONGRULESINTERFACE_H
