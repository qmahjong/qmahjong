#ifndef MAHJONG_TILELIST_H
#define MAHJONG_TILELIST_H

#include "mahjong_global.h"
#include "tile.h"

#include <QList>
#include <QMetaType>

class MAHJONGSHARED_EXPORT TileList : public QList<Tile*>
{
public:
    /*! Construct an empty list of tiles. */
    TileList() : QList<Tile*>() {}
    /*! QList<Tile*> implicit converter and copy constructor. */
    TileList(const QList<Tile*> &tileList) : QList<Tile*>(tileList) {}

    /*! Sort the list of tiles. */
    TileList& sort() { qStableSort(begin(), end(), Tile::lessThan); return *this; }

    /*! Shuffles the list of tiles. */
    TileList& shuffle() { qSort(begin(), end(), Tile::randomLessThan); return *this; }

    QString toString() const;

    /*! QString type converter. */
    operator QString() const { return toString(); }

    static TileList getSuitOf(Tile::Category category, QObject *parent = 0, Tile::factory factory = 0);
};

Q_DECLARE_METATYPE(TileList)

#endif // MAHJONG_TILELIST_H
