#include "action.h"

#include <QSharedData>

/*!
  \class Action
  \ingroup MahjongCore

  \brief The Action class provides a way to store possible actions to
  later present to the player.

  An Action is decribed through its type and the tile required to
  perform the action. A list of Action is generally precomputed in the
  background and queried when a tile becomes available to the player.
  Certain actions have limitations on the origin of the tile.

  \sa Tile
*/

/*!
  \enum Action::Type

  This enum describes the type of the action to perform. Some actions
  may have limitations on the origin of the wait-tile.

  \section Hand Openner

  Certain actions have the effect of opening your hand and revealing
  part of your tiles to other players. The Pong and the Chi always
  reveals the completed set to the other players, opening your hand at
  the same time. The Kan may or may not open your hand. When the Kan is
  performed on a discard, the set revealed and the hand opened. The Kan
  can also be declared on a self-draw, in which case, two tiles of the
  set (usually the middle tiles) are shown, but the hand is considered
  to remain concealed. The winning player must always show his hand to
  confirm his win.

  \section Action Priority

  More than one player may be able to perform an action on a tile at the
  same time. When this happens, claiming a tile to complete a winning
  hand takes precedence over any other actions and claiming a tile for a
  Kan or a Pong takes precedence over a Chi. When two player have the
  same action priority, the player who is the first next in turn has the
  priority. A claim with a higher priority can overthrow a claim that has
  just been made if it is declared within three seconds. Samewise, if a
  claim is declared after a draw but within three seconds, the picked
  tile replaced in the wall and the claim is performed.
*/

class Action::ActionData : public QSharedData
{
public:
    ActionData() {}
    ActionData(const ActionData &other) :
            QSharedData(other),
            waitTile(other.waitTile),
            actionType(other.actionType)
    {}
    ~ActionData() {}

    int waitTile;
    Action::Type actionType;
};

/*!
  Construct an invalid action.
*/
Action::Action() :
        d(new ActionData)
{
}

/*!
  Construct an action that is a copy of \a other.
*/
Action::Action(const Action &other) :
        d(other.d)
{
}

/*!
  Assigns a copy of \a action and returns a reference to this action.
*/
Action& Action::operator=(const Action &action)
{
    d = action.d;
    return *this;
}

/*!
  Destroy the action.
*/
Action::~Action()
{
}

/*!
  Returns the \link Tile::value value\endlink of the tile required to
  perform this action.

  \sa setWaitTile
*/
int Action::waitTile() const
{
    return d->waitTile;
}


/*!
  Sets the value of the tile required to perform this action to \a value.

  \sa waitTile
*/
void Action::setWaitTile(int value)
{
    d->waitTile = value;
}

/*!
  Returns the \link Action::Type type\endlink of action this action performs.

  \sa setActionType
*/
Action::Type Action::actionType() const
{
    return d->actionType;
}

/*!
  Sets the \link Action::Type type\endlink of action this action performs to
  \a actionType.

  \sa actionType
*/
void Action::setActionType(Type actionType)
{
    d->actionType = actionType;
}
