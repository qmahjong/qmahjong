#include "tilelist.h"

/*!
  \class TileList
  \ingroup MahjongCore

  \brief The TileList class is a container for a list of tiles.

  The class TileList extends QList<Tile*> with a sort and a shuffle
  function as well as a factory function.

  \sa Tile
*/

static Tile* baseFactory(Tile::Category cat, Tile::Variety var, QObject *parent)
{
    return new Tile(cat, var, parent);
}

/*!
  Factory function for creating all the tiles of a certain category.

  \param category The category of tile to create.
  \param parent The \link PlayingMat playing mat\endlink where the tiles will be used.
  \param factory A factory function for creating the tiles.
  \return A set of tiles of the category \a category.
*/
TileList TileList::getSuitOf(Tile::Category category, QObject *parent, Tile::factory factory)
{
    if (!factory)
        factory = baseFactory;

    TileList list;
    switch (category)
    {
    case Tile::Circle:
    case Tile::Bamboo:
    case Tile::Character:
        for (int i = 1; i < 10; ++i)
        {
            for (int e = 0; e < 4; ++e)
            {
                list << factory(category, Tile::Variety(i), parent);
            }
        }
        break;

    case Tile::Dragon:
        for (int e = 0; e < 4; ++e)
        {
            list << factory(category, Tile::White, parent);
            list << factory(category, Tile::Green, parent);
            list << factory(category, Tile::Red, parent);
        }
        break;

    case Tile::Wind:
        for (int e = 0; e < 4; ++e)
        {
            list << factory(category, Tile::East, parent);
            list << factory(category, Tile::South, parent);
            list << factory(category, Tile::West, parent);
            list << factory(category, Tile::North, parent);
        }
        break;

    case Tile::Season:
    case Tile::Flower:
        list << factory(category, Tile::East, parent);
        list << factory(category, Tile::South, parent);
        list << factory(category, Tile::West, parent);
        list << factory(category, Tile::North, parent);
        break;

    default: break;
    }

    return list;
}

/*!
  Returns a textual representation of the tiles in this list.
*/
QString TileList::toString() const
{
    QString text;

    foreach (Tile *tile, *this)
    {
        text += tile->toString();
        text += "; ";
    }

    text.chop(1);
    return text;
}
