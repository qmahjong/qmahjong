#ifndef MAHJONG_MAHJONG_H
#define MAHJONG_MAHJONG_H

#include "mahjong_global.h"

#include <QString>
#include <QList>
#include <QMap>
#include <QEvent>

class QScriptEngine;

class MahjongRulesInterface;
class MahjongGame;
class Player;
class Tile;

class MAHJONGSHARED_EXPORT Mahjong
{
public:
    typedef QList<MahjongRulesInterface*> RuleList;

    /*! Widgets provided by rules. */
    enum RuleWidget
    {
        RW_SettingsPage /*!< Page for the settings dialog. */
    };

    /*! Mahjong event types. */
    enum EventType
    {
        E_Invalid = QEvent::User,
        E_Draw,
        E_Discard
    };

    virtual ~Mahjong();

    RuleList getRules() const;

    static void registerScriptMetaTypes(QScriptEngine *engine);
    static void registerMetaTypes();

    /*! Returns the Mahjong singleton. */
    static Mahjong* instance() { return s_instance ? s_instance
                                                   : new Mahjong(); }

    static int rollDie6();

private:
    typedef QMap<QString, MahjongRulesInterface*> RuleMap;

    /*! List of loaded mahjong rules. */
    RuleMap m_rules;

    /*! Private singelton constructor. */
    Mahjong();
    /*! Disabled copy constructor. */
    Mahjong(const Mahjong &);
    /*! Disabled assignement operator. */
    Mahjong& operator=(const Mahjong &);

    /*! Load plugins from the `rules' directory. */
    void loadRulesPlugins();

    /*! Singleton instance. */
    static Mahjong *s_instance;
};

#define gMahjong (Mahjong::instance())

struct DrawEvent : public QEvent
{
    DrawEvent(Player *p, Tile *t) :
            QEvent(QEvent::Type(Mahjong::E_Draw)),
            player(p), tile(t) {}

    Player *player;
    Tile *tile;
};

struct DiscardEvent : public QEvent
{
    DiscardEvent(Player *p, Tile *t) :
            QEvent(QEvent::Type(Mahjong::E_Discard)),
            player(p), tile(t) {}

    Player *player;
    Tile *tile;
};

#endif // MAHJONG_MAHJONG_H
