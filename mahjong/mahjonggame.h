#ifndef MAHJONG_MAHJONGGAME_H
#define MAHJONG_MAHJONGGAME_H

#include "mahjong_global.h"

#include <QObject>
#include <QMetaType>
#include <QList>
#include <QReadWriteLock>
#include <QSemaphore>

class PlayingMat;
class Player;

class QStateMachine;
class QEvent;

class MAHJONGSHARED_EXPORT MahjongGame : public QObject
{
    Q_OBJECT
    Q_PROPERTY(PlayingMat* mat READ playingMat)

public:
    explicit MahjongGame(QObject *parent = 0);
    virtual ~MahjongGame();

    virtual PlayingMat* playingMat() const;
    virtual QList<Player*> players() const;

    QStateMachine* machine();

    void postGameEvent(QEvent *event, bool delayed = true);

public slots:
    virtual void initialize() = 0;

    bool update();
    void lockUpdate(int n = 1);
    bool unlockUpdate(int n = 1);

    Player* player(int) const;

signals:
    void updated();

protected:
    /*! Read/Write lock for thread synchronisation. */
    mutable QReadWriteLock m_rwlock;

    /*! This game's state machine. */
    QStateMachine *m_machine;

private:
    QSemaphore m_updateSemaphore;
};

Q_DECLARE_METATYPE(MahjongGame*)

#endif // MAHJONG_MAHJONGGAME_H
