#ifndef MAHJONG_GLOBAL_H
#define MAHJONG_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(MAHJONG_LIBRARY)
#  define MAHJONGSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MAHJONGSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // MAHJONG_GLOBAL_H
