#include "tile.h"
#include "mahjonggame.h"

#include <QReadLocker>
#include <QWriteLocker>

/*!
  \class Tile
  \ingroup MahjongCore

  \brief The Tile class describes a tile as used in Mahjong.

  Mahjong is usally played with a set of 136 or 144 tiles.
  Each tile has a category and a veriety which differentiates it from
  the others. Most tiles exist in four copy, with the exception of the
  Season and Flower tiles.

  \sa PlayingMat Wall Hand
*/

/*!
  \enum Tile::Category

  The Mahjong tiles are divided into three groups, the basic tiles,
  the honor tiles, and the bonus tiles.

  The basic tiles consists of the Circle, the Bamboo, and the Word tiles.
  Each of these basic sets are made of four suits running from 1 to 9.

  The honor tiles are the three Dragons and the four Winds.
  The Wind tiles represent the wind of each seats.
  \todo More details on Dragon tiles.

  The bonus tiles are the flowers and the seasons.
  They are not used in the Japanese variant of the game.

  \sa Variety categoryText shortCategoryText
*/

/*!
  \enum Tile::Variety

  This enum describes the different varieties of tiles.
  The basic tiles range from 1 to 9.
  The variety of winds represent the four cardinal points,
  \b East, \b North, \b West, and \b South.
  The dragon exist in the \b White, \b Green, and \b Red variety.

  \sa Category
*/

/*!
  \enum Tile::Facing

  This enum is used to tell whether a tile's face is hidden or visible.
*/

/*!
  \typedef Tile::factory

  A pointer to a factory function for creating new specialized tile.
  It is used by TileList::getSuitOf.
*/

/*!
  \property Tile::category

  This property holds the category this tile belongs to.
*/

/*!
  \property Tile::variety

  This property holds this tile's variety.
*/

/*!
  \property Tile::value

  This property holds this tile's value.
*/

/*!
  \property Tile::text

  This property holds a textual representation of this tile.
*/

/*!
  \property Tile::shortText

  This property holds a short textual representation of this tile.
*/

// SIGNAL(flipped(Tile::Facing))
/*!
  \fn Tile::flipped(Tile::Facing face)

  This signal is emmited whenever the tile is flipped from face down to
  face up and vice versa. The new facing of the tile is passed in the
  \a face parameter.

  \sa Facing flip
*/

/*!
  \var Tile::m_category

  This attribute holds this tile's \link Tile::Category category\endlink.
*/

/*!
  \var Tile::m_variety

  This attribute holds this tile's \link Tile::Variety variety\endlink.
*/

/*!
  \var Tile::m_facing

  This attribute holds this tile's \link Tile::Facing facing\endlink.
*/

/*!
  Construct a tile of category \a category and variety \a variety with
  \a parent as its parent.

  \param category The new tile's category.
  \param variety The new tile's variety.
  \param parent The tile's parent should be the \link PlayingMat playing
  mat\endlink where the tile will be used.

  \sa Category Variety
*/
Tile::Tile(Category category, Variety variety, QObject *parent) :
        QObject(parent),
        m_category(category),
        m_variety(variety),
        m_facing(FaceDown)
{
}

/*!
  Destroy the tile.
*/
Tile::~Tile()
{
}

/*!
  Returns the \link MahjongGame game\endlink this tile belongs to.
*/
MahjongGame* Tile::game() const
{
    return qobject_cast<MahjongGame*>(parent()->parent());
}

/*!
  \return the category of this tile.

  \sa Category setCategory
*/
Tile::Category Tile::category() const
{
    QReadLocker locker(&m_rwlock);

    return m_category;
}

/*!
  Sets the category of this tile to \a category.

  \sa Category category
*/
void Tile::setCategory(Category category)
{
    QWriteLocker locker(&m_rwlock);

    m_category = category;
}

/*!
  \return this tile's variety.

  \sa Variety setVariety
*/

Tile::Variety Tile::variety() const
{
    QReadLocker locker(&m_rwlock);

    return m_variety;
}

/*!
  Sets this tile's variety to \a variety.

  \sa Variety variety
*/
void Tile::setVariety(Variety variety)
{
    QWriteLocker locker(&m_rwlock);

    m_variety = variety;
}

/*!
  \return an integer representation of this tile
  based on this tile's category and variety.

  \sa category variety
*/
int Tile::value() const
{
    QReadLocker locker(&m_rwlock);

    return m_category | m_variety;
}

/*!
  Returns true if this tile is placed face up on the \link PlayingMat
  playing mat\endlink and visible to every player.

  \sa flip
*/
bool Tile::isFaceUp() const
{
    QReadLocker locker(&m_rwlock);

    return m_facing;
}

/*!
  Flip the tile, showing or hiding the tile to every player.
*/
void Tile::flip(Facing face)
{
    QWriteLocker locker(&m_rwlock);

    if (face == m_facing)
        return;

    m_facing = face;

    locker.unlock();
    emit flipped(face);
    game()->update();
}

/*!
  Returns true if this tile is an honor tile.

  \sa Category
*/
bool Tile::isHonor() const
{
    QReadLocker locker(&m_rwlock);

    switch (m_category)
    {
    case Wind:
    case Dragon:
        return true;

    default:
        return false;
    }
}

/*!
  Returns true if this tile is a terminal tile.
  Tiles which are considered to be terminal are the 1 and 9 of Circle,
  Bamboo, and Character.

  \sa Category Variety
*/
bool Tile::isTerminal() const
{
    QReadLocker locker(&m_rwlock);

    switch (m_variety)
    {
    case One:
    case Nine:
        return true;

    default:
        return false;
    }
}

/*!
  Retusn true if this tile is a bonus tile.

  \sa Category
*/
bool Tile::isBonus() const
{
    QReadLocker locker(&m_rwlock);

    switch (m_category)
    {
    case Season:
    case Flower:
        return true;

    default:
        return false;
    }
}

/*!
  Returns a text representing this tile's category.

  \sa Category shortCategoryText
*/
QString Tile::categoryText() const
{
    QReadLocker locker(&m_rwlock);

    switch (m_category)
    {
    case Circle:
        return tr("Circle", "Category Text");

    case Bamboo:
        return tr("Bamboo", "Category Text");

    case Character:
        return tr("Character", "Category Text");

    case Wind:
        return tr("Wind", "Category Text");

    case Dragon:
        return tr("Dragon", "Category Text");

    case Season:
        return tr("Season", "Category Text");

    case Flower:
        return tr("Flower", "Category Text");

    default: // to please gcc
        return tr("DEFAULT");
    }
}

/*!
  Returns a single letter as a diminutive of this tile's category text.

  \sa Category categoryText
*/
QString Tile::shortCategoryText() const
{
    QReadLocker locker(&m_rwlock);

    switch (m_category)
    {
    case Circle:
        return tr("D", "Circles");

    case Bamboo:
        return tr("B", "Bamboos");

    case Character:
        return tr("C", "Characters");

    case Wind:
        return tr("W", "Winds");

    case Dragon:
        return tr("D", "Dragons");

    case Season:
        return tr("S", "Seasons");

    case Flower:
        return tr("F", "Flowers");

    default: // to please gcc
        return tr("DEFAULT");
    }
}

/*!
  Returns a text representing this tile's variety.

  \sa Variety shortVarietyText
*/
QString Tile::varietyText() const
{
    QReadLocker locker(&m_rwlock);

    switch (m_category)
    {
    case Circle:
    case Bamboo:
    case Character:
        switch (m_variety)
        {
        case One:
            return tr("One");

        case Two:
            return tr("Two");

        case Three:
            return tr("Three");

        case Four:
            return tr("Four");

        case Five:
            return tr("Five");

        case Six:
            return tr("Six");

        case Seven:
            return tr("Seven");

        case Height:
            return tr("Height");

        case Nine:
            return tr("Nine");

        default: break;
        }
        break;

    case Wind:
        switch (m_variety)
        {
        case East:
            return tr("East");

        case South:
            return tr("South");

        case West:
            return tr("West");

        case North:
            return tr("North");

        default: break;
        }
        break;

    case Dragon:
        switch (m_variety)
        {
        case White:
            return tr("White");

        case Green:
            return tr("Green");

        case Red:
            return tr("Red");

        default: break;
        }
        break;

    case Season:
        switch (m_variety)
        {
        case East:
            //: Spring/Fisherman
            return tr("Spring");

        case South:
            //: Summer/Woodcutter
            return tr("Summer");

        case West:
            //: Autumn/Farmer
            return tr("Autumn");

        case North:
            //: Winter/Scholar
            return tr("Winter");

        default: break;
        }
        break;

    case Flower:
        switch (m_variety)
        {
        case East:
            return tr("Plum");

        case South:
            return tr("Orchid");

        case West:
            return tr("Chrysanthemum");

        case North:
            return tr("Bamboo", "Flower name");

        default: break;
        }
        break;

    default: break;
    }

    // to please gcc
    return  tr("DEFAULT");
}

/*!
  Returns a single letter as a diminutive of this tile's variety text.

  \sa Variety varietyText
*/
QString Tile::shortVarietyText() const
{
    QReadLocker locker(&m_rwlock);

    switch (m_category)
    {
    case Circle:
    case Bamboo:
    case Character:
        switch (m_variety)
        {
        case One:
            return tr("1", "One");

        case Two:
            return tr("2", "Two");

        case Three:
            return tr("3", "Three");

        case Four:
            return tr("4", "Four");

        case Five:
            return tr("5", "Five");

        case Six:
            return tr("6", "Six");

        case Seven:
            return tr("7", "Seven");

        case Height:
            return tr("8", "Height");

        case Nine:
            return tr("9", "Nine");

        default: break;
        }
        break;

    case Wind:
        switch (m_variety)
        {
        case East:
            return tr("E", "East");

        case South:
            return tr("S", "South");

        case West:
            return tr("W", "West");

        case North:
            return tr("N", "North");

        default: break;
        }
        break;

    case Dragon:
        switch (m_variety)
        {
        case White:
            return tr("[]", "White");

        case Green:
            return tr("G", "Green");

        case Red:
            return tr("R", "Red");

        default: break;
        }
        break;

    case Season:
        switch (m_variety)
        {
        case East:
            //: Spring/Fisherman
            return tr("P", "Spring");

        case South:
            //: Summer/Woodcutter
            return tr("S", "Summer");

        case West:
            //: Autumn/Farmer
            return tr("A", "Autumn");

        case North:
            //: Winter/Scholar
            return tr("W", "Winter");

        default: break;
        }
        break;

    case Flower:
        switch (m_variety)
        {
        case East:
            return tr("P", "Plum");

        case South:
            return tr("O", "Orchid");

        case West:
            return tr("C", "Chrysanthemum");

        case North:
            return tr("B", "Bamboo flower");

        default: break;
        }
        break;

    default: break;
    }

    // to please gcc
    return tr("DEFAULT");
}

/*!
  Returns a textual representation of this tile.

  \param colorize If this is set to \c true, the returned text will have
  its color styled by some html tags.

  \sa categoryText varietyText shortText
*/
QString Tile::toString(bool colorize) const
{
    QReadLocker locker(&m_rwlock);

    QString text;
    if (colorize)
        text = colorizeText();
    else
        text = "%1";

    switch (m_category)
    {
    case Circle:
    case Bamboo:
    case Character:
        //: text for the three basic tiles
        return text.arg(tr("%1 of %2").arg(varietyText()).arg(categoryText()));

    case Wind:
        //: text for the wind tiles
        return text.arg(tr("%1", "Wind").arg(varietyText()));

    case Dragon:
        //: text for the dragon tiles
        return text.arg(tr("%1 Dragon", "Dragon").arg(varietyText()));

    case Season:
        //: text for the season tiles
        return text.arg(tr("%1", "Season").arg(varietyText()));

    case Flower:
        //: text for the flower tiles
        return text.arg(tr("%1", "Flower").arg(varietyText()));

    default: break;
    }

    return "";
}

/*!
  Returns a short textual representation of this tile, made of no more
  than a few characters.

  \param colorize If this is set to \c true, the returned text will have
  its color styled by some html tags.

  \sa shortCategoryText shortVarietyText toString
*/
QString Tile::shortText(bool colorize) const
{
    QReadLocker locker(&m_rwlock);

    QString text;
    if (colorize)
        text = colorizeText();
    else
        text = "%1";

    switch (m_category)
    {
    case Circle:
    case Bamboo:
    case Character:
        //: short text for the three basic tiles
        return text.arg(tr("%1%2", "short basic tile text").arg(shortCategoryText()).arg(shortVarietyText()));

    case Wind:
        //: short text for the wind tiles
        return text.arg(tr("%1", "short wind text").arg(shortVarietyText()));

    case Dragon:
        //: short text for the dragon tiles
        return text.arg(tr("%1", "short dragon text").arg(shortVarietyText()));

    case Season:
        //: short text for the season tiles
        return text.arg(tr("%1%2", "short season text").arg(shortVarietyText()).arg(shortCategoryText()));

    case Flower:
        //: short text for the flower tiles
        return text.arg(tr("%1%2", "short flower text").arg(shortVarietyText()).arg(shortCategoryText()));

    default: break;
    }

    return "";
}

/*!
  Used to create the html tag which will add color to the text
  returned by toString and shortText.
*/
QString Tile::colorizeText() const
{
    switch (m_category)
    {
    case Bamboo:
        return "<span style=\"color: #008000;\">%1</span>";

    case Character:
        return "<span style=\"color: #ff0000;\">%1</span>";

    case Dragon:
        if(m_variety == White)
            return "<span style=\"color: #cccccc;\">%1</span>";
        if(m_variety == Green)
            return "<span style=\"color: #008000;\">%1</span>";
        if(m_variety == Red)
            return "<span style=\"color: #ff0000;\">%1</span>";

    default: break;
    }

    return "<span style=\"color: #000000;\">%1</span>";
}

bool Tile::randomLessThan(const Tile * /* lhs */, const Tile * /* rhs */)
{
    return qrand() < qrand();
}
