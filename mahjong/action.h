#ifndef MAHJONG_ACTION_H
#define MAHJONG_ACTION_H

#include "mahjong_global.h"

#include <QSharedDataPointer>

class MAHJONGSHARED_EXPORT Action
{
    class ActionData;

public:

    enum Type
    {
        NO_ACTION, /*!< An invalid Action. */
        Pong, /*!< Completing a set of three identical tiles. */
        Pung = Pong, /*!< Chinese equivalent of a Pong. */
        Kan, /*!< Completing a set of four identical tiles. */
        Kong = Kan, /*!< Chinese equivalent of a Kan. */
        Chi, /*!< Completing a sequence of three \link Tile::Category
                  basic tiles\endlink. This action can only be performed
                  on the discard of the preceding player
                  (i.e. the player to your left). */
        Chow = Chi, /*!< Chinese equivalent of a Chi. */
        Tsumo, /*!< Completing a winning hand on self-draw. */
        Mahjong = Tsumo, /*!< Chinese equivalent of completing a winning hand. */
        Ron /*!< Completing a winning hand on a discard. */
    };

    Action();
    Action(const Action &other);
    Action& operator=(const Action &other);
    virtual ~Action();

    int waitTile() const;
    void setWaitTile(int value);

    Type actionType() const;
    void setActionType(Type actionType);

private:
    QSharedDataPointer<ActionData> d;
};

#endif // MAHJONG_ACTION_H
