#include "playingmat.h"

#include "mahjonggame.h"
#include "wall.h"

#include <QReadLocker>
#include <QWriteLocker>

/*!
  \class PlayingMat
  \ingroup MahjongCore

  \todo Document the PlayingMat class.
*/

PlayingMat::PlayingMat(QObject *parent) :
        QObject(parent)
{
}

PlayingMat::~PlayingMat()
{
}

MahjongGame* PlayingMat::game() const
{
    return qobject_cast<MahjongGame*>(parent());
}

Wall* PlayingMat::wall() const
{
    return findChild<Wall*>();
}

Tile* PlayingMat::createTile(int category, int variety)
{
    QWriteLocker locker(&m_rwlock);

    return new Tile(static_cast<Tile::Category>(category),
                    static_cast<Tile::Variety>(variety),
                    this);
}

TileList PlayingMat::createSuitOf(int category)
{
    QWriteLocker locker(&m_rwlock);

    return TileList::getSuitOf(static_cast<Tile::Category>(category), this);
}

void PlayingMat::setPrepared(bool on)
{
    if (on)
        emit prepared();
}
