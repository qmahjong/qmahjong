#ifndef MAHJONG_PLAYER_H
#define MAHJONG_PLAYER_H

#include "mahjong_global.h"
#include "action.h"

#include <QObject>
#include <QMetaType>
#include <QString>
#include <QMultiHash>
#include <QReadWriteLock>

class MahjongGame;
class PlayingMat;
class Hand;
class Tile;

class QState;

class MAHJONGSHARED_EXPORT Player : public QObject
{
    Q_OBJECT
    Q_PROPERTY(MahjongGame* game READ game)
    Q_PROPERTY(PlayingMat* mat READ playingMat)
    Q_PROPERTY(Hand* hand READ hand)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(Seat seat READ seat WRITE setSeat NOTIFY seatChanged)
    Q_PROPERTY(Type type READ type WRITE setType)
    Q_ENUMS(Type)
    Q_ENUMS(Seat)
    Q_ENUMS(WinningState)

public:
    /*! The type of player. (i.e. how the player is controlled) */
    enum Type
    {
        Invalid, /*!< Uncontrolled player. */
        Human, /*!< Human controlled local player. */
        Computer, /*!< AI controlled local player. */
        Network /*!< Player controlled over the network. */
    };

    /*! The seats where the player can sit. */
    enum Seat
    {
        NOT_SEATED,
        East = 0x11,
        South = 0x22,
        West = 0x44,
        North = 0x88
    };

    /*! The winning states the player can be in. */
    enum WinningState
    {
        Noten, /*!< Not in a position to win. */
        Tenpai, /*!< Waitting on a tile to win. */
        Furiten, /*!< Waitting on a self-draw to win. */
        DEAD_HAND = -1 /*!< Irregularities in the player's hand. */
    };

    explicit Player(QObject *parent = 0);
    virtual ~Player();

    virtual MahjongGame* game() const; /*!< \sa game */
    virtual PlayingMat* playingMat() const;

    QString name() const; /*!< \sa name */
    void setName(const QString &name);

    Hand* hand() const; /*!< \sa hand */


    Seat seat() const; /*!< \sa seat */
    void setSeat(Seat seat);

    Type type() const; /*!< \sa type */
    void setType(Type type);

    Q_INVOKABLE virtual bool isDealer() const;
    Q_INVOKABLE virtual bool isLocalHuman() const;

    /*! Returns \c true if this player is seated to the left of \a other. */
    bool isLeftOf(const Player *other) const
    { return m_seat & other->m_seat>>1; }
    /*! Returns \c true if this player is seated to the right of \a other. */
    bool isRightOf(const Player *other) const
    { return m_seat>>1 & other->m_seat; }

    /*! Used to sort the players based on their seat. */
    static bool seatSort(const Player *lhs, const Player *rhs)
    { return lhs->m_seat < rhs->m_seat; }

public slots:
    virtual void draw();
    virtual void discard(Tile *tile);

signals:
    void nameChanged(QString);
    void seatChanged(Seat);

    void idleEntered();
    void idleExited();
    void drawEntered();
    void drawExited();
    void discardEntered();
    void discardExited();

protected:
    /*! Read/Write lock for thread syncronization. */
    mutable QReadWriteLock m_rwlock;

    /*! The player's name. */
    QString m_name;
    /*! The player's seat. */
    Seat m_seat;
    /*! The player's type. */
    Type m_type;

    /*! Pre-computed action options. */
    QMultiHash<int, Action> m_cachedOptions;
};

Q_DECLARE_METATYPE(Player*)

#endif // MAHJONG_PLAYER_H
