#include "hand.h"
#include "mahjonggame.h"
#include "playingmat.h"
#include "player.h"
#include "wall.h"

#include <QReadLocker>
#include <QWriteLocker>

/*!
  \class Hand
  \ingroup MahjongCore

  \brief The Hand class stores the player's tiles.

  A hand holds the player's tiles including the tiles he discarded.

  \sa Player Tile
*/

/*!
  \property Hand::tiles

  This property holds this hand's closed and opened tiles.
*/

/*!
  \property Hand::closedTiles

  This property holds this hand's closed tiles.
*/

/*!
  \property Hand::openedTiles

  This property holds this hand's opened tiles.
*/

/*!
  \property Hand::discardedTiles

  This property holds the tiles which were discarded from this hand.
*/

/*!
  \property Hand::riichiTile

  This property holds the tile which was discarded when this hand
  declared Riichi.
*/

/*!
  \fn Hand::changed

  This signal is emitted whenever the hand's tiles are modified.

  \sa giveTiles drawTile discardTile sortTiles
*/

/*!
  Construct an empty Hand.
*/
Hand::Hand(QObject *parent) :
        QObject(parent)
{
}

/*!
  Destroy a Hand.
*/
Hand::~Hand()
{
}

/*!
  Returns the \link MahjongGame game\endlink where this hand is in play.
*/
MahjongGame* Hand::game() const
{
    return qobject_cast<MahjongGame*>(parent()->parent());
}

/*!
  Returns the player this hand belongs to.
*/
Player* Hand::player() const
{
    return qobject_cast<Player*>(parent());
}

/*!
  \return this hand's closed and opened tiles.

  \sa closedTiles openedTiles
*/
TileList Hand::tiles() const
{
    QReadLocker locker(&m_rwlock);

    return m_closed + m_opened;
}

/*!
  \return this hand's closed tiles.
*/
TileList Hand::closedTiles() const
{
    QReadLocker locker(&m_rwlock);

    return m_closed;
}

/*!
  \return this hand's opened tiles.
*/
TileList Hand::openedTiles() const
{
    QReadLocker locker(&m_rwlock);

    return m_opened;
}

/*!
  \return the tiles which were discarded from this hand.
*/
TileList Hand::discardedTiles() const
{
    QReadLocker locker(&m_rwlock);

    return m_discarded;
}

/*!
  Returns \c true if this hand is not entirely concealed.
*/
bool Hand::isOpened() const
{
    QReadLocker locker(&m_rwlock);

    return m_opened.count() ? true : false;
}

/*!
  Returns \c true if this hand has declared Riichi.

  \sa riichiTile
*/
bool Hand::isRiichi() const
{
    QReadLocker locker(&m_rwlock);

    return m_riichiTile ? true : false;
}

/*!
  \return the tile which was discarded when the hand declared Riichi.

  \sa setRiichiTile isRiichi
*/
Tile *Hand::riichiTile() const
{
    QReadLocker locker(&m_rwlock);

    return m_riichiTile;
}

/*!
  Sets the tile which was discarded when the hand declared Riichi to \a tile.

  \sa riichiTile
*/
void Hand::setRiichiTile(Tile *tile)
{
    QWriteLocker locker(&m_rwlock);

    m_riichiTile = tile;
}

/*!
  Returns a textual representation of the hand. This is mainly useful for
  debugging.
*/
QString Hand::toString() const
{
    QReadLocker locker(&m_rwlock);

    return tr("\nClosed{ %1 }\nOpened{ %2 }\nDiscarded{ %3 }\n")
            .arg(m_closed).arg(m_opened).arg(m_discarded);
}

/*!
  Add the tiles \a tiles to this hand's closed tiles.

  This function emits Hand::changed.
*/
void Hand::giveTiles(TileList tiles)
{
    QWriteLocker locker(&m_rwlock);

    m_closed << tiles;

    locker.unlock();
    emit changed();
    game()->update();
}

/*!
  Add the tile \a tile to this hand's closed tiles.

  This function emits Hand::changed.
*/
void Hand::drawTile(Tile *tile)
{
    QWriteLocker locker(&m_rwlock);

    m_closed << tile;

    locker.unlock();
    emit changed();
    game()->update();
}

/*!
  Remove the tile \a tile from this this hand's closed tiles and place
  it in this hand's discarded tiles.

  This function emits Hand::changed.
*/
void Hand::discardTile(Tile *tile)
{
    QWriteLocker locker(&m_rwlock);

    m_discarded << tile;
    m_closed.removeOne(tile);

    locker.unlock();
    emit changed();
    tile->flip();
    game()->update();
}

/*!
  Sorts this hand's closed tiles.

  This function emits Hand::changed.
*/
void Hand::sortTiles()
{
    QWriteLocker locker(&m_rwlock);

    m_closed.sort();

    locker.unlock();
    emit changed();
    game()->update();
}
