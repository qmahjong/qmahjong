#ifndef MAHJONG_WALL_H
#define MAHJONG_WALL_H

#include "mahjong_global.h"
#include "tile.h"
#include "tilelist.h"

#include <QObject>
#include <QMetaType>
#include <QReadWriteLock>

class MahjongGame;
class PlayingMat;

class MAHJONGSHARED_EXPORT Wall : public QObject
{
    Q_OBJECT
    Q_PROPERTY(TileList tiles READ tiles WRITE setTiles)

public:
    explicit Wall(QObject *parent = 0);
    ~Wall();

    virtual MahjongGame* game() const;
    virtual PlayingMat* playingMat() const;

    TileList tiles() const; /*!< \sa tiles */
    void setTiles(TileList tiles);

    TileList deadWall() const;

    bool canDraw() const;

    Tile* takeAt(int i);
    Tile* takeFirst();
    Tile* takeLast();

public slots:
    void shuffle();
    void breakWall(int position);
    void setDeadWall(int n);

signals:
    void shuffled();
    void broken();
    void deadWallChanged();
    void depleted();

protected:
    /*! Read/Write lock for thread syncronization. */
    mutable QReadWriteLock m_rwlock;

    TileList m_tiles;
    int m_deadWallLength;
};

Q_DECLARE_METATYPE(Wall*)

#endif // MAHJONG_WALL_H
