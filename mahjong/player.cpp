#include "player.h"

#include "mahjong.h"
#include "mahjonggame.h"
#include "playingmat.h"
#include "wall.h"
#include "hand.h"

#include <QReadLocker>
#include <QWriteLocker>

/*!
  \class Player
  \ingroup MahjongCore

  \brief The Player class describes a %Mahjong player.
*/

/*!
  \property Player::game

  This property holds the game this player is playing.
*/

/*!
  \property Player::mat

  This property holds the playing mat on which this player is playing.

  \sa playingMat
*/

/*!
  \property Player::hand

  This property holds this player's hand.
*/

/*!
  \property Player::name

  This property holds this player's name.
*/

/*!
  \property Player::seat

  This property holds this player's seat.
*/

/*!
  \property Player::type

  This property holds this player's type.
*/

/*!
  Construct a Player.
*/
Player::Player(QObject *parent) :
        QObject(parent),
        m_name(tr("Player")),
        m_seat(NOT_SEATED),
        m_type(Invalid)
{
}

/*!
  Destroy a Player.
*/
Player::~Player()
{
}

/*!
  \return the game this player is playing.
*/
MahjongGame* Player::game() const
{
    return qobject_cast<MahjongGame*>(parent());
}

/*!
  Returns the \link PlayingMat playing mat\endlink where the player is
  seated.
*/
PlayingMat* Player::playingMat() const
{
    return parent()->findChild<PlayingMat*>();
}

/*!
  \return the player's name.

  \sa setName
*/
QString Player::name() const
{
    QReadLocker locker(&m_rwlock);

    return m_name;
}

/*!
  Sets the player's name to \a name.

  \sa name
*/
void Player::setName(const QString &name)
{
    QWriteLocker locker(&m_rwlock);

    m_name = name;

    locker.unlock();
    emit nameChanged(m_name);
}

/*!
  \return this player's hand.
*/
Hand* Player::hand() const
{
    return findChild<Hand*>();
}

/*!
  \return this player's seat.

  \sa setSeat
*/
Player::Seat Player::seat() const
{
    QReadLocker locker(&m_rwlock);

    return m_seat;
}

/*!
  Sets this player's seat to \a seat.

  \sa seat
*/
void Player::setSeat(Seat seat)
{
    QWriteLocker locker(&m_rwlock);

    m_seat = seat;

    locker.unlock();
    emit seatChanged(m_seat);
}

/*!
  \return this player's type.

  \sa setType
*/
Player::Type Player::type() const
{
    QReadLocker locker(&m_rwlock);

    return m_type;
}

/*!
  Sets this player's type to \a type.

  \sa type
*/
void Player::setType(Type type)
{
    QWriteLocker locker(&m_rwlock);

    m_type = type;
}

/*!
  Returns \c true if this player is the dealer.
*/
bool Player::isDealer() const
{
    return East == m_seat;
}

/*!
  Returns \c true if this player is localy controlled by a human.
*/
bool Player::isLocalHuman() const
{
    QReadLocker locker(&m_rwlock);

    return Human == m_type;
}

/*!
  Tells this player to draw a tile from the wall.
*/
void Player::draw()
{
    Wall *wall = parent()->findChild<Wall*>();
    Tile *tile = wall->takeFirst();

    if (tile)
    {
        hand()->drawTile(tile);
        game()->postGameEvent(new DrawEvent(this, tile));
    }
}

/*!
  Tells this player to discard the tile \a tile.
*/
void Player::discard(Tile *tile)
{
    hand()->discardTile(tile);
    game()->postGameEvent(new DiscardEvent(this, tile));
}
