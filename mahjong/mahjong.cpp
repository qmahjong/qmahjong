#include "mahjong.h"
#include "mahjongrulesinterface.h"

#include "tile.h"
#include "tilelist.h"
#include "hand.h"
#include "player.h"
#include "wall.h"
#include "playingmat.h"
#include "mahjonggame.h"

#include <QApplication>
#include <QDir>
#include <QPluginLoader>
#include <QtScript>
#include <QMetaType>

#ifdef QMJ_DEBUG
#define RULES_DEBUG_DIR 1
#else
#define RULES_DEBUG_DIR 0
#endif // QMJ_DEBUG

template <class T>
static QScriptValue mahjongObjectToScript(QScriptEngine *engine, T * const &in)
{ return engine->newQObject(in); }

template <class T>
static void mahjongObjectFromScript(const QScriptValue &object, T * &out)
{ out = qobject_cast<T*>(object.toQObject()); }

/*
template <class T>
static QScriptValue mahjongObjectScriptCtor(QScriptContext *context, QScriptEngine *engine)
{
    return QScriptValue();
}
*/

/*!
  \defgroup MahjongCore Mahjong's core classes.
*/

/*!
  \class Mahjong
  \ingroup MahjongCore

  \brief The Mahjong class is a singleton which loads game rules.

  \sa MahjongRulesInterface MahjongGame
*/

/*!
  \typedef Mahjong::RuleList

  Defines QList<MahjongRulesInterface*> to be a list of rules.
*/

Mahjong* Mahjong::s_instance = 0;

/*!
  Construct the Mahjong singleton.
*/
Mahjong::Mahjong()
{
    Mahjong::s_instance = this;

    loadRulesPlugins();
}

/*!
  Destroy the Mahjong singleton.
*/
Mahjong::~Mahjong()
{
    Mahjong::s_instance = 0;
}

/*!
  Returns a list of %Mahjong rules.

  \sa MahjongRulesInterface
*/
Mahjong::RuleList Mahjong::getRules() const
{
    return m_rules.values();
}

/*!
  Registers the %Mahjong meta-types inside the script engine \a engine.
*/
void Mahjong::registerScriptMetaTypes(QScriptEngine *engine)
{
    qScriptRegisterMetaType<Tile*>(engine, mahjongObjectToScript<Tile>,
                                           mahjongObjectFromScript<Tile>);
    QScriptValue qsMetaTile = engine->newQMetaObject(&Tile::staticMetaObject);
    engine->globalObject().setProperty("Tile", qsMetaTile);

    qScriptRegisterSequenceMetaType<TileList>(engine);

    qScriptRegisterMetaType(engine, mahjongObjectToScript<Hand>,
                                    mahjongObjectFromScript<Hand>);

    qScriptRegisterMetaType(engine, mahjongObjectToScript<Player>,
                                    mahjongObjectFromScript<Player>);
    QScriptValue qsMetaPlayer = engine->newQMetaObject(&Player::staticMetaObject);
    engine->globalObject().setProperty("Player", qsMetaPlayer);

    qScriptRegisterMetaType(engine, mahjongObjectToScript<Wall>,
                                    mahjongObjectFromScript<Wall>);

    qScriptRegisterMetaType(engine, mahjongObjectToScript<PlayingMat>,
                                    mahjongObjectFromScript<PlayingMat>);

    qScriptRegisterMetaType(engine, mahjongObjectToScript<MahjongGame>,
                                    mahjongObjectFromScript<MahjongGame>);
}

/*!
  Registers the %Mahjong meta-types for Signal/Slot connections.
*/
void Mahjong::registerMetaTypes()
{
    qRegisterMetaType<Tile*>();
    qRegisterMetaType<TileList>();
}

/*!
  Returns the result of rolling a six face dice.
*/
int Mahjong::rollDie6()
{
    return (qrand() % 6) + 1;
}

/*!
  Loads the rule plugins from the rules directory.
*/
void Mahjong::loadRulesPlugins()
{
    QDir rulesDir(qApp->applicationDirPath());
    rulesDir.cd("rules");

    if (RULES_DEBUG_DIR)
    {
        rulesDir.cd("DEBUG");
    }

    foreach(QString fileName, rulesDir.entryList(QDir::Files))
    {
        QPluginLoader loader(rulesDir.absoluteFilePath(fileName));
        QObject * const plugin = loader.instance();
        if (plugin)
        {
            MahjongRulesInterface * const rule =
                    qobject_cast<MahjongRulesInterface*>(plugin);

            if (rule)
            {
                m_rules.insert(rule->rulesName(), rule);
            }
        }
    }
}
