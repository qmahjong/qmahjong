#include "mahjonggame.h"
#include "delayedevent.h"

#include "playingmat.h"
#include "player.h"

#include <QReadLocker>
#include <QWriteLocker>
#include <QStateMachine>
#include <QCoreApplication>

/*!
  \class MahjongGame
  \ingroup MahjongCore

  \brief The MahjongGame class implements the core functionality of a
  %Mahjong game.
*/

/*!
  \property MahjongGame::mat

  This property holds this game's playing mat.

  \sa playingMat
*/

/*!
  \fn MahjongGame::updated

  This signal is memitted whenever a game update is completed.
*/

/*!
  \fn virtual void MahjongGame::initialize() = 0

  Initializes the game for a new match.
*/

/*!
  Construct a MahjongGame with only a state-machine.
*/
MahjongGame::MahjongGame(QObject *parent) :
        QObject(parent),
        m_machine(new QStateMachine(this))
{
    m_machine->setObjectName("GameFSM");
}

/*!
  Destroy the MahjongGame.
*/
MahjongGame::~MahjongGame()
{
}

/*!
  Returns this game's \link PlayingMat playing mat\endlink.
*/
PlayingMat* MahjongGame::playingMat() const
{
    return findChild<PlayingMat*>();
}

/*!
  Returns a list of this game's players.
*/
QList<Player*> MahjongGame::players() const
{
    QList<Player*> p = findChildren<Player*>();
    qSort(p.begin(), p.end(), Player::seatSort);
    return p;
}

/*!
  Returns this game's state machine.
*/
QStateMachine* MahjongGame::machine()
{
    return m_machine;
}

/*!
  Posts the event \a event to this game's state machine.
*/
void MahjongGame::postGameEvent(QEvent *event, bool delayed)
{
    if (!delayed)
    {
        m_machine->postEvent(event);
        return;
    }

    DelayedEvent *later = new DelayedEvent(event, m_machine, this);
    connect(this, SIGNAL(updated()),
            later, SLOT(activate()), Qt::QueuedConnection);
    update();
}

/*!
  Try to update the game.

  This functions emits MahjongGame::updated if no updates were locked.

  \return \c true if the game was updated.

  \sa lockUpdate unlockUpdate
*/
bool MahjongGame::update()
{
    if (m_updateSemaphore.available())
        return false;

    emit updated();
    return true;
}

/*!
  Locks \a n updates.

  \sa unlockUpdate
*/
void MahjongGame::lockUpdate(int n)
{
    m_updateSemaphore.release(n);
}

/*!
  Unlocks \a n updates and call MahjongGame::update.

  \return \c true if there were at least \a n locked updates.

  \sa lockUpdate
*/
bool MahjongGame::unlockUpdate(int n)
{
    bool r = m_updateSemaphore.tryAcquire(n);

    update();

    return r;
}

/*!
  Returns the player \a i.
*/
Player* MahjongGame::player(int i) const
{
    return players().value(i);
}

DelayedEvent::DelayedEvent(QEvent *e, QStateMachine *m, QObject *p) :
        QObject(p),
        m_event(e),
        m_machine(m)
{
}

void DelayedEvent::activate()
{
    m_machine->postEvent(m_event);
    deleteLater();
}
