#ifndef MAHJONG_PLAYINGMAT_H
#define MAHJONG_PLAYINGMAT_H

#include "mahjong_global.h"
#include "tile.h"
#include "tilelist.h"

#include <QObject>
#include <QMetaType>
#include <QString>
#include <QReadWriteLock>

class MahjongGame;
class Wall;

class MAHJONGSHARED_EXPORT PlayingMat : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Wall* wall READ wall)

public:
    explicit PlayingMat(QObject *parent = 0);
    virtual ~PlayingMat();

    virtual MahjongGame* game() const;

    virtual Wall* wall() const;

public slots:
    Tile* createTile(int tileCategory, int tileVariety);
    TileList createSuitOf(int tileCategory);
    void setPrepared(bool on = true);

signals:
    void prepared();

protected:
    /*! Read/Write lock for thread synchronisation. */
    mutable QReadWriteLock m_rwlock;
};

Q_DECLARE_METATYPE(PlayingMat*)

#endif // MAHJONG_PLAYINGMAT_H
