#ifndef PRECOMPILE_H
#define PRECOMPILE_H

#include <QObject>
#include <QString>
#include <QList>
#include <QLibrary>
#include <QPluginLoader>
#include <QReadWriteLock>
#include <QReadLocker>
#include <QWriteLocker>
#include <QSharedData>
#include <QSharedDataPointer>
#include <QMap>
#include <QMultiHash>
#include <QMetaType>
#include <QApplication>
#include <QDir>

#endif // PRECOMPILE_H
