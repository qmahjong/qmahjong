INCLUDEPATH += \
    $$PWD/include \
    $$PWD/mahjong \
    $$PWD/Model \
    $$PWD/Control \
    $$PWD/Gui

unix:!isEmpty(QMAKE_RPATHDIR): QMAKE_LFLAGS+= $${QMAKE_LFLAGS_RPATH}.

CONFIG *= debug_and_release
CONFIG -= exceptions

!contains(QT,testlib){
    CONFIG += precompile_header
    PRECOMPILED_HEADER = precompile.h
    HEADERS += precompile.h
}

SLIBDIR = $$PWD/slib
staticlib{
    DESTDIR = $$SLIBDIR
    CONFIG *= create_prl
}

CONFIG(debug, debug|release){
    CONFIGNAME = "DEBUG"
    DEFINES += QMJ_DEBUG
}else{
    CONFIGNAME = "RELEASE"
    DEFINES += QMJ_RELEASE
}

TMPDIR = $$(BUILDDIR)
isEmpty( TMPDIR ) {
    TMPDIR = $$quote($$PWD/.tmp/$${TARGET}_$${CONFIGNAME})
} else {
    TMPDIR = $$quote($$(BUILDDIR)/.tmp/$${TARGET}_$${CONFIGNAME})
}
OBJECTS_DIR = $${TMPDIR}
MOC_DIR = $${TMPDIR}
UI_DIR = $${TMPDIR}
RCC_DIR = $${TMPDIR}

!win32:CONFIG(debug, debug|release){
    unix: TARGET = $$join(TARGET,,,_debug)
    else: TARGET = $$join(TARGET,,,d)
}
win32:build_pass:CONFIG(debug, debug|release){
    unix: TARGET = $$join(TARGET,,,_debug)
    else: TARGET = $$join(TARGET,,,d)
}

win32{
    contains(TEMPLATE,app): TEMPLATE = vcapp
    contains(TEMPLATE,lib): TEMPLATE = vclib
}

!isEmpty(DEPLIBS){
    build_pass:CONFIG(debug, debug|release)    {
        unix: DEPLIBS ~= s/([a-z0-9_]+)/\1_debug/g
        else: DEPLIBS ~= s/([a-z0-9_]+)/\1d/g
    }
    LIBS += -L$$quote($$PWD/) $$join(DEPLIBS," -l",-l,)
}

!isEmpty(DEPSLIB){
    CONFIG *= link_prl
    build_pass:CONFIG(debug, debug|release)    {
        unix: DEPSLIB ~= s/([a-z0-9_]+)/\1_debug/g
        else: DEPSLIB ~= s/([a-z0-9_]+)/\1d/g
    }
    TMPVAR = $$DEPSLIB
    win32: TMPVAR ~= s/([a-z0-9_]+)/\1.lib/g
    else: TMPVAR ~= s/([a-z0-9_]+)/lib\1.a/g
    LIBS += -L$$quote($$SLIBDIR) $$join(DEPSLIB," -l",-l,)
    PRE_TARGETDEPS += $$join(TMPVAR," $$SLIBDIR/",$$SLIBDIR/,)
}
