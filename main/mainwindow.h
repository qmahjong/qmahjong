#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>

class QDialog;

namespace Ui
{
    class MainWindow;
}

/*!
  \brief The Main window.

  The Main window which contains everything else.
*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MainWindow *ui;
    QDialog *m_settingsDialog;
    QWidget *m_centralWidget;

private slots:
    void on_action_About_QMahjong_triggered();
    void on_action_Configure_QMahjong_triggered();
    void on_action_New_triggered();
};

#endif // MAINWINDOW_H
