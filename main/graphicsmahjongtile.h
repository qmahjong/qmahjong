#ifndef GRAPHICSMAHJONGTILE_H
#define GRAPHICSMAHJONGTILE_H

#include <QGraphicsObject>

class GraphicsMahjongTile : public QGraphicsObject
{
    Q_OBJECT

public:
    explicit GraphicsMahjongTile(QGraphicsItem *parent = 0);
    ~GraphicsMahjongTile();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);

protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void wheelEvent(QGraphicsSceneWheelEvent *event);

private:
    QGraphicsTextItem *m_text;

};

#endif // GRAPHICSMAHJONGTILE_H
