#ifndef CENTRALWIDGET_H
#define CENTRALWIDGET_H

#include <QGraphicsView>
#include <QImage>

class QPixmap;

class CentralWidget : public QGraphicsView
{
    Q_OBJECT
    Q_PROPERTY(bool keepAspectRatio READ keepAspectRatio WRITE setKeepAspectRatio)

public:
    explicit CentralWidget(QWidget *parent = 0);
    ~CentralWidget();

    bool keepAspectRatio() const;

public slots:
    void setKeepAspectRatio(bool on);

protected:
    void resizeEvent(QResizeEvent *event);
    void drawBackground(QPainter *painter, const QRectF &rect);

private:
    QGraphicsScene *m_scene;
    QPixmap *m_background;
    bool m_keepAspectRatio;

    static const QImage backgroundImage();
};

#endif // CENTRALWIDGET_H
