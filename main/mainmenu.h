#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>

namespace Ui
{
    class MainMenu;
}

class MainMenu : public QWidget
{
    Q_OBJECT

public:
    explicit MainMenu(QWidget *parent = 0);
    ~MainMenu();

signals:
    void singlePlayerClicked();
    void multiPlayerClicked();
    void optionsClicked();

protected:
    void changeEvent(QEvent *e);
    void paintEvent(QPaintEvent *);

private:
    Ui::MainMenu *ui;
};

#endif // MAINMENU_H
