#!/bin/sh
echo '<!DOCTYPE RCC><RCC version="1.0">' > icons.qrc
echo '<qresource>' >> icons.qrc
find icons/ -type f -print | sort | sed -e 's/.*/\t<file>&<\/file>/' >> icons.qrc
echo '</qresource>' >> icons.qrc
echo '</RCC>' >> icons.qrc
echo '<!DOCTYPE RCC><RCC version="1.0">' > images.qrc
echo '<qresource>' >> images.qrc
find images/ -type f -print | sort | sed -e 's/.*/\t<file>&<\/file>/' >> images.qrc
echo '</qresource>' >> images.qrc
echo '</RCC>' >> images.qrc
