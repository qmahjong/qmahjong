#ifndef GRAPHICSACTIONDIALOG_H
#define GRAPHICSACTIONDIALOG_H

#include <QGraphicsWidget>

class GraphicsActionDialog : public QGraphicsWidget
{
    Q_OBJECT

public:
    explicit GraphicsActionDialog(QGraphicsItem *parent = 0);
    ~GraphicsActionDialog();

    void paintWindowFrame(QPainter *painter,
                          const QStyleOptionGraphicsItem *option,
                          QWidget *widget);
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget);

protected:
    //bool event(QEvent *event);
};

#endif // GRAPHICSACTIONDIALOG_H
