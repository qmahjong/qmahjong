#include "graphicsmahjongtile.h"

#include <QPainter>
#include <QGraphicsTextItem>
#include <QGraphicsEffect>
#include <QGraphicsSceneWheelEvent>

GraphicsMahjongTile::GraphicsMahjongTile(QGraphicsItem *parent) :
        QGraphicsObject(parent),
        m_text(new QGraphicsTextItem(this))
{
    setFlags(QGraphicsItem::ItemIsMovable |
             QGraphicsItem::ItemIsSelectable);
    setAcceptHoverEvents(true);

    m_text->setHtml("<span style='color:blue;'>MJ</span>");
    m_text->setPos(-m_text->boundingRect().center());
    m_text->hide();
}

GraphicsMahjongTile::~GraphicsMahjongTile()
{
}

QRectF GraphicsMahjongTile::boundingRect() const
{
    return QRectF(QPointF(-15, -22.5), QSize(30, 45));
}

void GraphicsMahjongTile::paint(QPainter *painter,
                                const QStyleOptionGraphicsItem * /*option*/,
                                QWidget * /*widget*/)
{
    painter->save();

    if (graphicsEffect())
    {
        painter->setPen(QColor::fromRgbF(0.2, 0.6, 1, 0.8));
    }
    else
    {
        painter->setPen(Qt::NoPen);
    }

    if (1)
    {
        QRadialGradient gradient(0, 0, 45, -9, -16);
        gradient.setColorAt(0, Qt::white);
        gradient.setColorAt(0.05, QColor::fromRgbF(1, 0.85, 0.2));
        gradient.setColorAt(0.5, QColor::fromRgbF(1, 0.65, 0));
        gradient.setColorAt(0.75, QColor::fromRgbF(0.65, 0.45, 0));
        gradient.setColorAt(0.8, QColor::fromRgbF(0.2, 0.05, 0));
        gradient.setColorAt(0.9, Qt::black);
        painter->setBrush(QBrush(gradient));
    }
    else
    {
        QRadialGradient gradient(0, 0, 45, -9, -16);
        gradient.setColorAt(0, Qt::white);
        gradient.setColorAt(0.05, QColor::fromRgbF(0.85, 0.9, 0.9));
        gradient.setColorAt(0.5, QColor::fromRgbF(0.85, 0.85, 0.8));
        gradient.setColorAt(0.75, QColor::fromRgbF(0.65, 0.7, 0.65));
        gradient.setColorAt(0.8, QColor::fromRgbF(0.3, 0.3, 0.3));
        gradient.setColorAt(0.9, Qt::black);
        painter->setBrush(QBrush(gradient));
    }

    painter->drawRoundedRect(boundingRect(), 5, 5);

    painter->restore();
}

void GraphicsMahjongTile::hoverEnterEvent(QGraphicsSceneHoverEvent *)
{
    QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect();
    effect->setOffset(0);
    effect->setBlurRadius(10);
    effect->setColor(QColor::fromRgbF(0.2, 0.6, 1, 1));
    setGraphicsEffect(effect);
    update();
}

void GraphicsMahjongTile::hoverLeaveEvent(QGraphicsSceneHoverEvent *)
{
    setGraphicsEffect(0);
    update();
}

void GraphicsMahjongTile::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    rotate(event->delta()/12);
    event->accept();
}
