#include "graphicsactiondialog.h"

#include <QPainter>
#include <QStyle>
#include <QStyleOptionGraphicsItem>
#include <QPalette>
#include <QGraphicsScene>
#include <QEvent>
#include <QGraphicsSceneEvent>

GraphicsActionDialog::GraphicsActionDialog(QGraphicsItem *parent) :
        QGraphicsWidget(parent, Qt::Window | Qt::CustomizeWindowHint
                        | Qt::WindowTitleHint)
                        //| Qt::WindowCloseButtonHint
                        //| Qt::WindowSystemMenuHint)
{
    //setFlag(ItemIgnoresTransformations, true);
    resize(200, 200);
    setMinimumSize(50, 50);
    /*
    QPalette custom(palette());
    custom.setBrush(QPalette::Window, QBrush(QColor::fromRgbF(0, 0.8, 0, 0.5)));
    custom.setBrush(QPalette::Button, QBrush(QColor::fromRgbF(1, 0, 0, 1)));
    setPalette(custom);
    */
}

GraphicsActionDialog::~GraphicsActionDialog()
{}

void GraphicsActionDialog::paintWindowFrame(QPainter *painter,
        const QStyleOptionGraphicsItem * /*option*/,
        QWidget * /*widget*/)
{
    /*
    QStyleOptionGraphicsItem myOption(*option);
    myOption.palette = palette();
    QGraphicsWidget::paintWindowFrame(painter, &myOption, widget);
    return;
    */
    painter->save();

    QPainterPath path;
    path.addRoundedRect(windowFrameRect(), 5, 5);
    path.addRect(rect());

    painter->setBrush(QBrush(QColor::fromRgbF(0.3, 0.3, 0.3, 0.7)));
    painter->setPen(Qt::NoPen);
    painter->drawPath(path);

    /*
    QStyleOptionTitleBar bar;
    bar.text = windowTitle();
    bar.titleBarFlags = windowFlags();
    bar.palette = widget->palette();
    bar.rect = windowFrameRect().toRect();
    bar.rect.setHeight(
            style()->pixelMetric(QStyle::PM_TitleBarHeight, &bar, widget));
    //style()->drawComplexControl(QStyle::CC_TitleBar, &bar, painter, widget);

    QRect closeButtonRect(style()->subControlRect(
            QStyle::CC_TitleBar, &bar, QStyle::SC_TitleBarCloseButton, widget));

    painter->drawPixmap(closeButtonRect,
                        style()->standardPixmap(QStyle::SP_TitleBarCloseButton, &bar, widget));
    */

    painter->restore();
}

void GraphicsActionDialog::paint(QPainter *painter,
        const QStyleOptionGraphicsItem * /*option*/,
        QWidget * /*widget*/)
{
    painter->save();

    painter->setBrush(QColor::fromRgbF(0.7, 0.7, 0.7, 0.5));
    painter->drawRect(rect());

    painter->restore();
}

/*
bool GraphicsActionDialog::event(QEvent *event)
{
    if (event->type() == QEvent::GraphicsSceneMouseMove)
    {
        QGraphicsSceneMouseEvent *mouse =
                static_cast<QGraphicsSceneMouseEvent*>(event);
        if (!scene()->sceneRect().contains(mouse->scenePos()))
        {
            mouse->ignore();
            return false;
        }
    }

    return QGraphicsWidget::event(event);
}
*/
