#include "gamewidget.h"

#include "Mahjong"
#include "Gui/GameGraphics"

#include <QPainter>
#include <QPixmap>
#include <QImage>
#include <QResizeEvent>
#include <QBoxLayout>
#include <QGraphicsView>
#include <QGraphicsScene>

#include <QUiLoader>
#include <QtScript>
#include <QDir>
#include <QTextStream>
#include <QMessageBox>
#include <QCoreApplication>

GameWidget::GameWidget(QWidget *parent) :
        QWidget(parent),
        m_background(0),
        m_view(0),
        m_interpreter(0)
{
    setMinimumSize(640, 480);

    QGraphicsScene *gameScene = new QGraphicsScene(-400, -400,
                                                    800,  800, this);
    gameScene->setItemIndexMethod(QGraphicsScene::NoIndex);
    m_view = new QGraphicsView(gameScene);
    m_view->setStyleSheet("padding: 0px;\n"
                          "margin: 0px;\n"
                          "border-width: 1px;\n"
                          "border-radius: 10px;\n"
                          "border-style: solid;\n"
                          "border-color: white;\n"
                          "background-color: rgba(100%, 100%, 100%, 20%);\n");
    m_view->viewport()->setWindowFlags(Qt::FramelessWindowHint);
    m_view->viewport()->setAttribute(Qt::WA_TranslucentBackground, true);
    m_view->viewport()->setStyleSheet("QToolTip {\n"
                                      "padding: 3px;\n"
                                      "margin: 0px;\n"
                                      "border-width: 1px;\n"
                                      "border-radius: 0px;\n"
                                      "border-style: solid;\n"
                                      "border-color: black;\n"
                                      "background-color: rgb(240, 230, 140);\n"
                                      "opacity: 200;\n"
                                      "}\n");
    m_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_view->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    m_view->setRenderHints(QPainter::Antialiasing |
                           QPainter::TextAntialiasing |
                           QPainter::SmoothPixmapTransform);

    MahjongGame *game = gMahjong->getRules().at(0)->getGame();
    game->setParent(this);
    //GameGraphics *graphics =
    new GameGraphics(game, gameScene);

    QBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(m_view);
    setLayout(layout);
    QTimer::singleShot(0, this, SLOT(fitView()));
    QTimer::singleShot(15, this, SLOT(fitView())); // in case there is a lag
    QTimer::singleShot(500, game, SLOT(initialize()));
}

GameWidget::~GameWidget()
{
    delete m_background;
}

bool GameWidget::loadScript(const QString &file)
{
    if (!m_interpreter)
    {
        m_interpreter = new QScriptEngine(this);
        Mahjong::registerScriptMetaTypes(m_interpreter);
        m_interpreter->globalObject().setProperty("gameWidget",
                       m_interpreter->newQObject(this));
        m_interpreter->globalObject().setProperty("game",
                       m_interpreter->newQObject(findChild<MahjongGame*>()));
    }

    QFile jsFile(file);
    jsFile.open(QFile::ReadOnly);
    QTextStream in(&jsFile);
    QString script = in.readAll();
    jsFile.close();

    m_interpreter->evaluate(script);
    if (m_interpreter->hasUncaughtException())
    {
        QMessageBox messageBox(this);
        messageBox.setIcon(QMessageBox::Warning);
        messageBox.setWindowTitle(tr("GameWidget::loadScript(...)"));
        messageBox.setText(tr("An error occured while executing the "
                              "script %1.").arg(file));
        messageBox.setInformativeText(tr("%1.")
                      .arg(m_interpreter->uncaughtException().toString()));
        messageBox.setDetailedText(m_interpreter->uncaughtExceptionBacktrace()
                                   .join("\n"));
        messageBox.exec();

        return false;
    }

    return true;
}

QWidget* GameWidget::loadWidget(const QString &uiFile)
{
    QUiLoader loader;
    QFile file(uiFile);
    file.open(QFile::ReadOnly);

    QWidget *widget = loader.load(&file, this);
    file.close();

    if (widget)
        layout()->addWidget(widget);

    return widget;
}

void GameWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.drawPixmap(0, 0, *m_background);
}

void GameWidget::resizeEvent(QResizeEvent *event)
{
    static QImage image(":images/table.png");

    delete m_background;
    m_background = new QPixmap(event->size());
    QPainter painter(m_background);
    painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
    painter.drawImage(m_background->rect(), image);

    m_view->fitInView(m_view->sceneRect(), Qt::KeepAspectRatio);

    QWidget::resizeEvent(event);
}

void GameWidget::fitView()
{
    m_view->fitInView(m_view->sceneRect(), Qt::KeepAspectRatio);
}
