#include "mainscene.h"
#include "mainmenu.h"

#include "graphicsmahjongtile.h"
#include "graphicsactiondialog.h"

#include <QAction>
#include <QGraphicsProxyWidget>
#include <QStateMachine>
#include <QAbstractTransition>
#include <QPropertyAnimation>

#include <QGraphicsRectItem>
#include <QPen>

#include <QDebug>

MainScene::MainScene(QObject *parent) :
        QGraphicsScene(parent),
        m_machine(new QStateMachine(this))
{
    setItemIndexMethod(NoIndex);
    setSceneRect(0, 0, 640, 480);

    // main menu
    MainMenu *menu = new MainMenu();
    menu->resize(450, 450);
    QGraphicsProxyWidget *menuProxy = addWidget(menu);
    menuProxy->setPos(sceneRect().center() - menuProxy->rect().center());
    menuProxy->setOpacity(0);

    // State
    // initial transparent state
    QState *s0 = new QState();
    // initial fade-in transition
    QState *sFadeIn = new QState();
    // root menu state
    QState *sRoot = new QState();
    // root fade-out to single player state
    QState *sRoot2Single = new QState();
    // single player player state
    QState *sSingle = new QState();

    // per state properties
    s0->assignProperty(menuProxy, "opacity", 0);
    sFadeIn->assignProperty(menuProxy, "opacity", 1);
    sRoot2Single->assignProperty(menuProxy, "opacity", 0);

    // transitions
    QAbstractTransition *transition;
    QPropertyAnimation *animation;
    // initial transition from 0 to root
    transition = s0->addTransition(sFadeIn);
    animation = new QPropertyAnimation(menuProxy, "opacity");
    animation->setDuration(2000);
    transition->addAnimation(animation);
    sFadeIn->addTransition(sFadeIn, SIGNAL(propertiesAssigned()),
                           sRoot);
    // transition from root to single player
    transition = sRoot->addTransition(menu, SIGNAL(singlePlayerClicked()),
                                      sRoot2Single);
    animation = new QPropertyAnimation(menuProxy, "opacity");
    animation->setDuration(500);
    transition->addAnimation(animation);
    sRoot2Single->addTransition(sRoot2Single, SIGNAL(propertiesAssigned()),
                                sSingle);
    connect(sSingle, SIGNAL(entered()), SLOT(startSinglePlayer()));

    // open the options when requested
    connect(menu, SIGNAL(optionsClicked()), SLOT(openOptions()));

    // setup the state-machine
    m_machine->addState(s0);
    m_machine->addState(sFadeIn);
    m_machine->addState(sRoot);
    m_machine->addState(sRoot2Single);
    m_machine->addState(sSingle);
    m_machine->setInitialState(s0);
    m_machine->start();
}

MainScene::~MainScene()
{}

void MainScene::startSinglePlayer()
{
    QObject *mainWindow = parent();
    while (!mainWindow->inherits("QMainWindow"))
        mainWindow = mainWindow->parent();

    QAction *actionNew = mainWindow->findChild<QAction*>("action_New");
    actionNew->trigger();
}

void MainScene::openOptions()
{
    QObject *mainWindow = parent();
    while (!mainWindow->inherits("QMainWindow"))
        mainWindow = mainWindow->parent();

    QAction *actionConfig = mainWindow->findChild<QAction*>("action_Configure_QMahjong");
    actionConfig->trigger();
}
