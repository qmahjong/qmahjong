#ifndef MAINSCENE_H
#define MAINSCENE_H

#include <QGraphicsScene>

class QStateMachine;

class MainScene : public QGraphicsScene
{
    Q_OBJECT

public:
    explicit MainScene(QObject *parent = 0);
    ~MainScene();

protected slots:
    void startSinglePlayer();
    void openOptions();

private:
    QStateMachine *m_machine;
};

#endif // MAINSCENE_H
