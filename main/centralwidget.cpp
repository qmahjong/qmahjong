#include "centralwidget.h"
#include "mainscene.h"

#include <QPixmap>

CentralWidget::CentralWidget(QWidget *parent) :
        QGraphicsView(parent),
        m_scene(new MainScene(this)),
        m_background(0),
        m_keepAspectRatio(false)
{
    setMinimumSize(640, 480);
    setStyleSheet("padding: 0px; border: 0px; margin: 0px;");
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setViewportUpdateMode(FullViewportUpdate);
    setRenderHints(QPainter::SmoothPixmapTransform |
                   QPainter::Antialiasing |
                   QPainter::TextAntialiasing);
    setDragMode(RubberBandDrag);
    setScene(m_scene);
}

CentralWidget::~CentralWidget()
{
    delete m_background;
}

bool CentralWidget::keepAspectRatio() const
{
    return m_keepAspectRatio;
}

void CentralWidget::setKeepAspectRatio(bool on)
{
    m_keepAspectRatio = on;
}

void CentralWidget::resizeEvent(QResizeEvent *event)
{
    delete m_background;

    m_background = new QPixmap(event->size());
    QPainter painter(m_background);
    painter.setRenderHint(QPainter::SmoothPixmapTransform);
    painter.drawImage(m_background->rect(), backgroundImage());

    if (m_keepAspectRatio && scene())
    {
        fitInView(scene()->sceneRect(), Qt::KeepAspectRatio);
    }
    QGraphicsView::resizeEvent(event);
}

void CentralWidget::drawBackground(QPainter *painter, const QRectF &rect)
{
    painter->save();
    painter->resetTransform();

    painter->drawPixmap(0, 0, *m_background);

    painter->restore();
    QGraphicsView::drawBackground(painter, rect);
}

const QImage CentralWidget::backgroundImage()
{
    static QImage image(":images/table.png");
    return image;
}
