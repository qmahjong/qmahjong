#include "Mahjong"
#include "mainwindow.h"

#include <QtGui/QApplication>
#include <QLocale>
#include <QSettings>
#include <QTranslator>
#include <QLibraryInfo>
#include <QMetaType>
#include <QRegExp>

int main(int argc, char *argv[])
{
    // set up the app and translator
    QApplication app(argc, argv);

    QSettings::setDefaultFormat(QSettings::IniFormat);
    app.setOrganizationName("QMahjong.org");
    app.setOrganizationDomain("qmahjong.org");
    app.setApplicationName("QMahjong");

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                     QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QTranslator mahjongTranslator;
    mahjongTranslator.load("qmahjong_" + QLocale::system().name());
    app.installTranslator(&mahjongTranslator);

    // register the MetaTypes
    Mahjong::registerMetaTypes();

    // set up the main window
    MainWindow window;
    window.show();

    return app.exec();
}

/*!
  \mainpage

  The documentation for QMahjong is work in progress.

  See \ref MahjongCore for more information on the most important classes
  of QMahjong.

*/
