#ifndef GAMEWIDGET_H
#define GAMEWIDGET_H

#include <QWidget>

class QPixmap;
class QGraphicsView;
class QScriptEngine;

class GameWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GameWidget(QWidget *parent = 0);
    ~GameWidget();

    bool loadScript(const QString &file);

public slots:
    QWidget *loadWidget(const QString &uiFile);

protected:
    void paintEvent(QPaintEvent *);
    void resizeEvent(QResizeEvent *e);

private:
    QPixmap *m_background;
    QGraphicsView *m_view;
    QScriptEngine *m_interpreter;

private slots:
    void fitView();

};

#endif // GAMEWIDGET_H
