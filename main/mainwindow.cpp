#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "Model/GameData"
#include "Gui/Settings"

#include "centralwidget.h"
#include "gamewidget.h"

#include <QDialog>
#include <QMessageBox>
#include <QGridLayout>
#include <QIcon>

#include <QApplication>
#include <QDesktopWidget>

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow),
        m_settingsDialog(0),
        m_centralWidget(0)
{
    ui->setupUi(this);
    m_centralWidget = new CentralWidget();
    ui->centralWidget->layout()->addWidget(m_centralWidget);

    GlobalSettings settings;
    ui->action_Debug->setVisible(settings.getDebugMenu());

    /* set the icons */
    ui->action_New->setIcon(
            QIcon::fromTheme("document-new", QIcon(":/icons/document-new.png")));
    ui->action_Load->setIcon(
            QIcon::fromTheme("document-open", QIcon(":/icons/document-open.png")));
    ui->action_Save->setIcon(
            QIcon::fromTheme("document-save", QIcon(":/icons/document-save.png")));
    ui->action_Save_As->setIcon(
            QIcon::fromTheme("document-save-as", QIcon(":/icons/document-save-as.png")));
    ui->action_Quit->setIcon(
            QIcon::fromTheme("application-exit", QIcon(":/icons/application-exit.png")));
    ui->action_Configure_QMahjong->setIcon(
            QIcon::fromTheme("configure", QIcon(":/icons/configure.png")));
    ui->action_About_QMahjong->setIcon(
            QIcon::fromTheme("help-about", QIcon(":/icons/help-about.png")));
    ui->action_About_Qt->setIcon(QIcon(":/icons/qt.svg"));

    connect(ui->action_About_Qt, SIGNAL(triggered()),
            qApp, SLOT(aboutQt()));

    // center in screen
    QRect screen(QApplication::desktop()->screenGeometry());
    move((screen.width()-width())/2,
         (screen.height()-height())/2);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::on_action_Configure_QMahjong_triggered()
{
    if (!m_settingsDialog)
    {
        m_settingsDialog = new SettingsDialog(this);
    }
    if (m_settingsDialog->isHidden())
    {
        m_settingsDialog->show();
    }
    else
    {
        m_settingsDialog->raise();
        m_settingsDialog->activateWindow();
    }
}

void MainWindow::on_action_About_QMahjong_triggered()
{
    QMessageBox::about(this, tr("About QMahjong"),
            tr("<h2>QMahjong</h2>"
               "<p>Copyright &copy; 2009 by Maxime Bouchard.</p>"
               "<p>More to come soon...</p>"));
}

void MainWindow::on_action_New_triggered()
{
    centralWidget()->layout()->removeWidget(m_centralWidget);
    m_centralWidget->deleteLater();
    m_centralWidget = new GameWidget();
    centralWidget()->layout()->addWidget(m_centralWidget);
}
