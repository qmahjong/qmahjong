TARGET = qmahjong
TEMPLATE = app
QT += script \
    network
CONFIG += uitools
DESTDIR = ..
DEPLIBS = mahjong
DEPSLIB = gamedata \
    gamegraphics \
    settings
include(../qmahjong.pri)
SOURCES += main.cpp \
    mainwindow.cpp \
    centralwidget.cpp \
    mainscene.cpp \
    mainmenu.cpp \
    graphicsmahjongtile.cpp \
    graphicsactiondialog.cpp \
    gamewidget.cpp
HEADERS += mainwindow.h \
    centralwidget.h \
    mainscene.h \
    mainmenu.h \
    graphicsmahjongtile.h \
    graphicsactiondialog.h \
    gamewidget.h
FORMS += mainwindow.ui \
    mainmenu.ui
RESOURCES += icons.qrc \
    images.qrc
win32:RC_FILE = qmahjong.rc
