#include "mainmenu.h"
#include "ui_mainmenu.h"

#include <QPainter>

MainMenu::MainMenu(QWidget *parent) :
        QWidget(parent, Qt::FramelessWindowHint),
        ui(new Ui::MainMenu)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->singlePlayer, SIGNAL(clicked()), SIGNAL(singlePlayerClicked()));
    connect(ui->multiPlayer, SIGNAL(clicked()), SIGNAL(multiPlayerClicked()));
    connect(ui->options, SIGNAL(clicked()), SIGNAL(optionsClicked()));
}

MainMenu::~MainMenu()
{
    delete ui;
}

void MainMenu::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainMenu::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setBrush(QBrush(QColor(128, 128, 128, 128)));
    painter.drawRoundedRect(rect().adjusted(1, 1, -1, -1), 10, 10);
}
