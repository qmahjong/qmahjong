#include "riichisettingspage.h"
#include "ui_riichisettingspage.h"

/*!
  \class RiichiSettingsPage

  \todo Document RiichiSettingsPage.
*/

RiichiSettingsPage::RiichiSettingsPage(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::RiichiSettingsPage)
{
    ui->setupUi(this);
    setObjectName("riichiSettingsPage");
}

RiichiSettingsPage::~RiichiSettingsPage()
{
    delete ui;
}

void RiichiSettingsPage::loadSettings()
{
}

void RiichiSettingsPage::saveSettings()
{
}

void RiichiSettingsPage::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
