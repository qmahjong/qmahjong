#ifndef RIICHISETTINGSPAGE_H
#define RIICHISETTINGSPAGE_H

#include <QWidget>

namespace Ui {
    class RiichiSettingsPage;
}

class RiichiSettingsPage : public QWidget
{
    Q_OBJECT

public:
    explicit RiichiSettingsPage(QWidget *parent = 0);
    ~RiichiSettingsPage();

public slots:
    void loadSettings();
    void saveSettings();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::RiichiSettingsPage *ui;
};

#endif // RIICHISETTINGSPAGE_H
