#ifndef RIICHIGAME_H
#define RIICHIGAME_H

#include "Mahjong"

class RiichiPlayer;

class QState;
class QAbstractState;

class RiichiGame : public MahjongGame
{
    Q_OBJECT

public:
    explicit RiichiGame(QObject *parent = 0);
    ~RiichiGame();

    void initialize();

private:
    void setUpdateTransition(QState *lhs, QAbstractState *rhs);

private slots:
    void on_game_finished();
};

#endif // RIICHIGAME_H
