#ifndef RIICHIRULESPLUGIN_H
#define RIICHIRULESPLUGIN_H

#include "Mahjong"

#include <QObject>
#include <QString>

class RiichiRulesPlugin : public QObject, public MahjongRulesInterface
{
    Q_OBJECT
    Q_INTERFACES(MahjongRulesInterface)

public:
    explicit RiichiRulesPlugin(QObject *parent = 0);
    ~RiichiRulesPlugin();

    QString rulesName() const;
    QString rulesShortName() const;

    QString rulesDesc() const;

    QWidget* getWidget(Mahjong::RuleWidget ruleWidget) const;

    MahjongGame* getGame();
};

#endif // RIICHIRULESPLUGIN_H
