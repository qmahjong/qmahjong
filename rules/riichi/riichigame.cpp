#include "riichigame.h"
#include "riichimat.h"
#include "riichiwall.h"
#include "riichiplayer.h"

#include <QStateMachine>
#include <QFinalState>
#include <QMessageBox>

/*!
  \class RiichiGame
  \ingroup RiichiRule

  \todo Document RiichiGame.
*/

namespace
{
    class StateBreakWall : public QState
    {
    public:
        explicit StateBreakWall(RiichiWall *wall, QState *parent = 0) :
                QState(parent), m_wall(wall)
        {
        }
    protected:
        void onEntry(QEvent *event)
        {
            int dice = Mahjong::rollDie6() + Mahjong::rollDie6() - 1;
            int i = (m_wall->tiles().count()/4 * ((4 * dice - dice) % 4))
                    + (dice * 2);
            m_wall->breakWall(i);
            QState::onEntry(event);
        }
    private:
        RiichiWall *m_wall;
    };

    class StateDeadWall : public QState
    {
    public:
        explicit StateDeadWall(RiichiWall *wall, QState *parent = 0) :
                QState(parent), m_wall(wall)
        {
        }
    protected:
        void onEntry(QEvent *event)
        {
            m_wall->setDeadWall(7 * 2);
            QState::onEntry(event);
        }
    private:
        RiichiWall *m_wall;
    };

    class StatePlayerDraw : public QState
    {
    public:
        explicit StatePlayerDraw(Player *player,
                                 int n = 1, QState *parent = 0) :
                QState(parent), m_player(player), m_n(n)
        {
        }
    protected:
        void onEntry(QEvent *event)
        {
            Wall *wall = m_player->game()->playingMat()->wall();
            TileList tiles;
            if (m_n != 1 || !m_player->isDealer())
            {
                for (int i = 0; i < m_n; ++i)
                    tiles << wall->takeFirst();
            }
            else
            {
                tiles << wall->takeFirst() << wall->takeAt(4);
            }
            m_player->hand()->giveTiles(tiles);
            QState::onEntry(event);
        }
    private:
        Player *m_player;
        int m_n;
    };
};

RiichiGame::RiichiGame(QObject *parent) :
        MahjongGame(parent)
{
    new RiichiMat(this);
    RiichiPlayer *east = new RiichiPlayer(this);
    RiichiPlayer *south = new RiichiPlayer(this);
    RiichiPlayer *west = new RiichiPlayer(this);
    RiichiPlayer *north = new RiichiPlayer(this);

    east->setName("P:East");
    east->setSeat(Player::East);
    east->setType(Player::Human);
    south->setName("P:South");
    south->setSeat(Player::South);
    south->setType(Player::Computer);
    west->setName("P:West");
    west->setSeat(Player::West);
    west->setType(Player::Computer);
    north->setName("P:North");
    north->setSeat(Player::North);
    north->setType(Player::Computer);
}

RiichiGame::~RiichiGame()
{
}

void RiichiGame::initialize()
{
    RiichiWall *wall = findChild<RiichiWall*>();

    QState *sSetUp = new QState();
    sSetUp->setObjectName("riichiSetUpState");
    QState *sShuffleWall = new QState(sSetUp);
    sSetUp->setInitialState(sShuffleWall);
    connect(sShuffleWall, SIGNAL(entered()), wall, SLOT(shuffle()));
    QState *sBreakWall = new StateBreakWall(wall, sSetUp);
    setUpdateTransition(sShuffleWall, sBreakWall);
    QState *sDeadWall = new StateDeadWall(wall, sSetUp);
    setUpdateTransition(sBreakWall, sDeadWall);
    QState *sTurnDora = new QState(sSetUp);
    setUpdateTransition(sDeadWall, sTurnDora);
    connect(sTurnDora, SIGNAL(entered()), wall, SLOT(turnDoraIndicator()));

    QList<Player*> players = MahjongGame::players();
    players.swap(1, 3);
    QState *lastState = sTurnDora;
    // distribute the tiles
    for (int i = 0; i < 3; ++i)
    {
        foreach (Player *player, players)
        {
            QState *sDraw = new StatePlayerDraw(player, 4, sSetUp);
            setUpdateTransition(lastState, sDraw);

            lastState = sDraw;
        }
    }

    foreach (Player *player, players)
    {
        QState *sDraw = new StatePlayerDraw(player, 1, sSetUp);
        setUpdateTransition(lastState, sDraw);

        lastState = sDraw;
    }

    QState *sSortPlayers = new QState(sSetUp);
    foreach (Player *player, players)
        connect(sSortPlayers, SIGNAL(entered()), player->hand(), SLOT(sortTiles()));
    setUpdateTransition(lastState, sSortPlayers);

    QFinalState *sSetUpFinal = new QFinalState(sSetUp);
    setUpdateTransition(sSortPlayers, sSetUpFinal);

    QState *sPlay = new QState(QState::ParallelStates);
    sPlay->setObjectName("riichiPlayState");
    sSetUp->addTransition(sSetUp, SIGNAL(finished()), sPlay);
    foreach (Player *player, players)
        qobject_cast<RiichiPlayer*>(player)->createStates(sPlay);

    QFinalState *sFinal = new QFinalState();
    sFinal->setObjectName("riichiFinalState");
    sPlay->addTransition(sPlay, SIGNAL(finished()), sFinal);

    // TODO: fix thing up
    sPlay->addTransition(wall, SIGNAL(depleted()), sFinal);

    m_machine->addState(sSetUp);
    m_machine->addState(sPlay);
    m_machine->addState(sFinal);
    m_machine->setInitialState(sSetUp);

    m_machine->start();

    connect(m_machine, SIGNAL(finished()),
            SLOT(on_game_finished()), Qt::QueuedConnection);
}

void RiichiGame::setUpdateTransition(QState *lhs, QAbstractState *rhs)
{
    lhs->addTransition(this, SIGNAL(updated()), rhs);
}

void RiichiGame::on_game_finished()
{
    QMessageBox::information(0, tr("Congratulation!"),
        tr("<h1>Congratulation!</h1>"
           "<p>You have completed the demo of <b>QMahjong</b>!</p>"));
}
