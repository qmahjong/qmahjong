# -------------------------------------------------
# Project created by QtCreator 2009-12-18T15:07:34
# -------------------------------------------------
TARGET = riichi
TEMPLATE = lib
DEPLIBS = mahjong
include(../../qmahjong.pri)
include(../rules.pri)
SOURCES += riichirulesplugin.cpp \
    riichisettingspage.cpp \
    riichigame.cpp \
    riichiplayer.cpp \
    riichimat.cpp \
    riichiwall.cpp
HEADERS += riichirulesplugin.h \
    riichisettingspage.h \
    riichigame.h \
    riichiplayer.h \
    riichimat.h \
    riichiwall.h
FORMS += riichisettingspage.ui
