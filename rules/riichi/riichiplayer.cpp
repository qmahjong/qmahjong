#include "riichiplayer.h"

#include <QStateMachine>
#include <QFinalState>
#include <QAbstractTransition>

/*!
  \class RiichiPlayer
  \ingroup RiichiRule

  \todo Document RiichiPlayer.
*/

namespace
{
    class IdleToDraw : public QAbstractTransition
    {
    public:
        explicit IdleToDraw(Player *p, QAbstractState *t = 0) :
                m_player(p)
        {
            if (t)
                setTargetState(t);
        }

    protected:
        virtual bool eventTest(QEvent *e)
        {
            if (e->type() != QEvent::Type(Mahjong::E_Discard))
                return false;
            DiscardEvent *discard = static_cast<DiscardEvent*>(e);
            return m_player->isRightOf(discard->player);
        }

        virtual void onTransition(QEvent *) {}

    private:
        Player *m_player;
    };

    class DrawToDiscard : public QAbstractTransition
    {
    public:
        explicit DrawToDiscard(Player *p, QAbstractState *t = 0) :
                m_player(p)
        {
            if (t)
                setTargetState(t);
        }

    protected:
        virtual bool eventTest(QEvent *e)
        {
            if (e->type() != QEvent::Type(Mahjong::E_Draw))
                return false;
            DrawEvent *draw = static_cast<DrawEvent*>(e);
            return m_player == draw->player;
        }

        virtual void onTransition(QEvent *) {}

    private:
        Player *m_player;
    };

    class DiscardToIdle : public QAbstractTransition
    {
    public:
        explicit DiscardToIdle(Player *p, QAbstractState *t = 0) :
                m_player(p)
        {
            if (t)
                setTargetState(t);
        }

    protected:
        virtual bool eventTest(QEvent *e)
        {
            if (e->type() != QEvent::Type(Mahjong::E_Discard))
                return false;
            DiscardEvent *discard = static_cast<DiscardEvent*>(e);
            return m_player == discard->player;
        }

        virtual void onTransition(QEvent *) {}

    private:
        Player *m_player;
    };
};

RiichiPlayer::RiichiPlayer(QObject *parent) :
        Player(parent)
{
    new Hand(this);
}

RiichiPlayer::~RiichiPlayer()
{
}

void RiichiPlayer::createStates(QState *parent)
{
    if (!isLocalHuman())
    {
        connect(this, SIGNAL(discardEntered()), SLOT(easyDiscard()));
        connect(this, SIGNAL(drawEntered()), SLOT(draw()));
    }

    QState *sRoot = new QState(parent);
    QState *sIdle = new QState(sRoot);
    QState *sDraw = new QState(sRoot);
    QState *sDiscard = new QState(sRoot);

    // Transitions from Root State
    if (isDealer())
        sRoot->setInitialState(sDiscard);
    else
        sRoot->setInitialState(sIdle);

    // Transitions from Idle State
    sIdle->addTransition(new IdleToDraw(this, sDraw));

    // Transitions from Draw State
    sDraw->addTransition(new DrawToDiscard(this, sDiscard));

    // Transitions from Discard State
    sDiscard->addTransition(new DiscardToIdle(this, sIdle));

    // connections
    connect(sIdle, SIGNAL(entered()), SIGNAL(idleEntered()), Qt::QueuedConnection);
    connect(sIdle, SIGNAL(exited()), SIGNAL(idleExited()), Qt::QueuedConnection);
    connect(sDraw, SIGNAL(entered()), SIGNAL(drawEntered()), Qt::QueuedConnection);
    connect(sDraw, SIGNAL(exited()), SIGNAL(drawExited()), Qt::QueuedConnection);
    connect(sDiscard, SIGNAL(entered()), SIGNAL(discardEntered()), Qt::QueuedConnection);
    connect(sDiscard, SIGNAL(exited()), SIGNAL(discardExited()), Qt::QueuedConnection);
    connect(sDiscard, SIGNAL(exited()), hand(), SLOT(sortTiles()), Qt::QueuedConnection);
}

void RiichiPlayer::easyDiscard()
{
    discard(hand()->closedTiles().shuffle().first());
}
