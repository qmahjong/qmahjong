#include "riichimat.h"
#include "riichiwall.h"

/*!
  \class RiichiMat
  \ingroup RiichiRule

  \todo Document RiichiMat.
*/

RiichiMat::RiichiMat(QObject *parent) :
        PlayingMat(parent)
{
    TileList tiles;
    tiles << TileList::getSuitOf(Tile::Circle, this);
    tiles << TileList::getSuitOf(Tile::Bamboo, this);
    tiles << TileList::getSuitOf(Tile::Character, this);
    tiles << TileList::getSuitOf(Tile::Wind, this);
    tiles << TileList::getSuitOf(Tile::Dragon, this);
    RiichiWall *wall = new RiichiWall(this);
    wall->setTiles(tiles);
}

RiichiMat::~RiichiMat()
{
}
