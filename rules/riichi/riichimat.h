#ifndef RIICHIMAT_H
#define RIICHIMAT_H

#include "Mahjong"

class RiichiMat : public PlayingMat
{
    Q_OBJECT

public:
    explicit RiichiMat(QObject *parent = 0);
    ~RiichiMat();
};

#endif // RIICHIMAT_H
