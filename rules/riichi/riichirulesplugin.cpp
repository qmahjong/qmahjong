#include "riichirulesplugin.h"
#include "riichisettingspage.h"
#include "riichigame.h"

/*!
  \defgroup RiichiRule Riichi Rules
*/

/*!
  \class RiichiRulesPlugin
  \ingroup RiichiRule

  \todo Document RiichiRulesPlugin.
*/

RiichiRulesPlugin::RiichiRulesPlugin(QObject *parent) :
        QObject(parent)
{
}

RiichiRulesPlugin::~RiichiRulesPlugin()
{
}

QString RiichiRulesPlugin::rulesName() const
{
    return tr("Japanese Riichi Competition Rules");
}

QString RiichiRulesPlugin::rulesShortName() const
{
    return tr("Riichi");
}

QString RiichiRulesPlugin::rulesDesc() const
{
    return tr("Japanese Riichi Competition Rules");
}

QWidget *RiichiRulesPlugin::getWidget(Mahjong::RuleWidget ruleWidget) const
{
    QWidget* widget;

    switch(ruleWidget)
    {
    case Mahjong::RW_SettingsPage:
        widget = new RiichiSettingsPage();
        break;

    default:
        widget = 0;
    }

    return widget;
}

MahjongGame *RiichiRulesPlugin::getGame()
{
    return new RiichiGame();
}

Q_EXPORT_PLUGIN2(riichi, RiichiRulesPlugin)
