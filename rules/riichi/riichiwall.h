#ifndef RIICHIWALL_H
#define RIICHIWALL_H

#include "Mahjong"

class RiichiWall : public Wall
{
    Q_OBJECT

public:
    RiichiWall(QObject *parent = 0);
    ~RiichiWall();

    TileList doraIndicators() const;

public slots:
    Tile* turnDoraIndicator();

private:
    TileList m_doraIndicators;
};

#endif // RIICHIWALL_H
