#ifndef RIICHIPLAYER_H
#define RIICHIPLAYER_H

#include "Mahjong"

class QState;

class RiichiPlayer : public Player
{
    Q_OBJECT

public:
    explicit RiichiPlayer(QObject *parent = 0);
    ~RiichiPlayer();

    void createStates(QState *parent);

public slots:
    void easyDiscard();
};

#endif // RIICHIPLAYER_H
