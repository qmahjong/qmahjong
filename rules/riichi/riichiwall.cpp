#include "riichiwall.h"

/*!
  \class RiichiWall
  \ingroup RiichiRule

  \todo Document RiichiWall.
*/

RiichiWall::RiichiWall(QObject *parent) :
        Wall(parent)
{
}

RiichiWall::~RiichiWall()
{
}

TileList RiichiWall::doraIndicators() const
{
    QReadLocker locker(&m_rwlock);

    return m_doraIndicators;
}

Tile* RiichiWall::turnDoraIndicator()
{
    QWriteLocker locker(&m_rwlock);

    TileList dead = m_tiles.mid(m_tiles.count() - m_deadWallLength);
    Tile *indicator = dead.at(4 + 2 * m_doraIndicators.count());
    m_doraIndicators << indicator;

    locker.unlock();
    indicator->flip();
    return indicator;
}
